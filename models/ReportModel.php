<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 08.09.16
 * Time: 11:44
 */

namespace app\models;


use yii\db\ActiveRecord;

class ReportModel extends ActiveRecord
{

    public function rules()
    {

        return [
            [['content', 'type'], 'required'],
        ];
    }


    public static function tableName()
    {
        return 'reports';
    }

}