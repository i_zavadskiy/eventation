<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 09.08.16
 * Time: 11:30
 */

namespace app\models;


use app\modules\events\models\Event;
use app\modules\events\models\UsersEvents;
use app\modules\users\models\Emailer;
use app\modules\users\models\User;
use yii\base\Model;

class RemindModel extends Model
{


    public function getEvents()
    {
        $events = Event::find()->all();
        $remind = [];
        foreach($events as $value) {
            $str1 = date("Ymd");
            $str2 = date("Ymd", strtotime($value['date_action']));
            if(($str1 + 3) == $str2) {
                $remind[] = $value;

            }
        }
        return $remind;
    }

    public function remind($event)
    {
        $find_users = UsersEvents::find()->where(['event_id' => $event["id"]])->all();
        echo $event['id']."\n";
        foreach($find_users as $value) {
            $user = User::find()->where(['id' => $value['user_id']])->one();
            Emailer::sendRemind($user['email'],$event['id']);
        }

    }


}