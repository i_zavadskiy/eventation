<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 06.10.16
 * Time: 10:47
 */

namespace app\models;

use Yii;

class ImageCrop
{
    protected $_uploadPass;
    private $_image;
    private $_type;
    private $_name;
    function __construct($name,$pass)
    {
        $this->_name = $name;
       $this->_type = substr ( $name, strripos ($name , '.')+1, 3 );

        if($this->_type == "png"){

            $this->_image = imagecreatefrompng ($pass.$name);
        }else{
            $this->_image = imagecreatefromjpeg($pass.$name);
        }
    }

    public function crop($x,$y,$wight,$height,$size){

        $new_img = imagecreatetruecolor($size, $size);
        imagecopyresampled(
            $new_img,
            $this->_image,
            0,
            0,
            $x,
            $y,
            $size,
            $size,
            $wight,
            $height
        );

        if($this->_type == "png"){
            imagepng($new_img, $this->_uploadPass.$this->_name);
        }else{

            imagejpeg($new_img, $this->_uploadPass.$this->_name);
        }

    }

}