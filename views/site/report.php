<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 01.09.16
 * Time: 13:28
 */
use yii\helpers\Html;
use \yii\widgets\ActiveForm;
?>
<?php if(Yii::$app->request->isGet):?>
<?php $form = ActiveForm::begin([
    'id' => 'report',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-8 col-md-offset-2\">{input}</div>\n<div class=\"col-lg-4 col-lg-offset-4\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]);
?>
<h1 align="center"><?= Yii::t('common','Report about problem') ?></h1>
<p align="center"><?= Yii::t('common','Fill the following fields to report about any problems.') ?></p>

<?=
$form->field($model, 'type')
    ->dropDownList([
        'Mistake' => Yii::t('common', 'Mistake'),
        'Bug' => Yii::t('common', 'Bug'),
        'Doesnt work' => Yii::t('common', 'Doesnt work'),
        'Other' => Yii::t('common', 'Other'),
    ])->label(false);
?>
<?= $form->field($model, 'content')->textarea(['rows' => 10, 'style' => ['resize' => 'none']])->label(false) ?>

<div hidden>
    <?= $form->field($model, 'url')->textInput(['value' => $url]) ?>
</div>


<div class=" form-group">
    <div class="col-lg-offset-5 col-lg-11">
        <?= Html::submitButton(Yii::t('common','Send'), ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'claim_b']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php endif;?>
