<?php


use yii\helpers\Url;

$this->registerJsFile('/js/LazyLoadEvents.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/owl.carousel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->registerCssFile('/css/owl.carousel.css');
function cutStr($str, $length = 50, $postfix = '...')
{
    if(strlen($str) <= $length)
        return $str;

    $temp = substr($str, 0, $length);
    if(strrpos($temp, ' ')) {
        return substr($temp, 0, strrpos($temp, ' ')).$postfix;
    } else {
        return $temp.$postfix;
    }
}

?>


<?php if($model == null) {
    echo "<h1 align='center'>There is no events yet</h1>";
} ?>



<div align="center">
<div class="owl-carousel">



    <?php foreach($model as $value):?>
    <div>
        <div class="example3">
        <a href="<?= Url::to(["/events/show/".$value['id']]) ?>">
        <?php if($value['img']==null):?>
            <img src="http://10.10.54.150/images/img.jpg" class="img-responsive"  alt="gf">
        <?php endif; ?>

        <?php if($value['img'] != null): ?>
            <img src="<?= $value->getMedium() ?>" class="img-responsive"  alt="ava">
        <?php endif; ?>
        </a>
            <div class="example_text">
                <h6><?= $value['name']?></h6>
            </div>
        </div>
    </div>
  <?php endforeach;?>
    </div>

</div>


<script>

    $(document).ready(function(){

        $(".owl-carousel").owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 2222,
            slideBy: 3,
            smartSpeed: 2000,
            items : 6,
            itemsScaleUp:true

        });
    });
</script>


<div class="container" style="padding-top: 30px;">
<?php foreach($event as $value): ?>
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="media">
                <a class="pull-left" href="<?= Url::to(["/events/show/".$value->id]) ?>">
                    <?php if($value->img != null): ?>
                        <img class="media-object" src="<?= $value->getThumb() ?>" style="max-width: 100px"
                             alt="ava">
                    <?php endif; ?>
                    <?php if($value->img == null): ?>
                        <img class="media-object" src="http://10.10.54.150/images/img.jpg"
                             style="max-width: 100px" alt="ava">
                    <?php endif; ?>

                </a>
                <div class="media-body row">


                    <h4 class="media-heading"><a
                            href="<?= Url::to(["/events/show/".$value->id]) ?>"><?= $value->name ?></a>
                    </h4>

                    <div class="col-md-6" id="open">
                        <span id="hd">
                       <?php
                       $event = htmlentities($value->content);
                       echo cutStr($event)
                       ?>

                        </span>

                        <div id="demo<?= $value->id ?>" class="collapse">
                            <?php
                            $event = htmlentities($value->content);
                            echo $event;
                            ?>

                        </div>
                        <a class="tog" style="cursor: pointer" data-toggle="collapse"
                           data-target="#demo<?= $value->id ?>">
                            <span data-toggle="tooltip" data-placement="right" title="<?=Yii::t('common', 'preview');?>" class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </div>


                    <p class="text-right"><?php

                        echo date('d-m-Y', strtotime($value->created_at));

                        ?>
                    </p>


                </div>
            </div>
        </div>
    </div>

<?php endforeach; ?>

<div class="out"></div>
</div>