<?php

use \yii\widgets\LinkPager;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Url;
use yii\grid\GridView;
use \yii\widgets\ListView;

$this->registerJsFile('/js/LazyLoadEvents.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/fullView.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
function cutStr($str, $length = 50, $postfix = '...')
{
    if(strlen($str) <= $length)
        return $str;

    $temp = substr($str, 0, $length);
    if(strrpos($temp, ' ')) {
        return substr($temp, 0, strrpos($temp, ' ')).$postfix;
    } else {
        return $temp.$postfix;
    }
}
?>


<div id="status">
</div>
<?php for($i = 0;
          $i <= (count($model) - 1);
          $i++): ?>
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="media">
                <a class="pull-left" href="<?= Url::to(["/events/show/".$model[$i]->id]) ?>">
                    <?php if($model[$i]->img != null): ?>
                        <img class="media-object" src="<?= $model[$i]->getThumb() ?>" style="max-width: 100px"
                             alt="ava">
                    <?php endif; ?>
                    <?php if($model[$i]->img == null): ?>
                        <img class="media-object" src="http://10.10.54.150/images/img.jpg"
                             style="max-width: 100px" alt="ava">
                    <?php endif; ?>

                </a>
                <div class="media-body row">


                    <h4 class="media-heading"><a
                            href="<?= Url::to(["/events/show/".$model[$i]->id]) ?>"><?= $model[$i]->name ?></a>
                    </h4>

                    <div class="col-md-6" id="open">
                        <span id="hd">
                       <?php
                       $event = htmlentities($model[$i]->content);
                       echo cutStr($event)
                       ?>

                        </span>

                        <div id="demo<?= $model[$i]->id ?>" class="collapse">
                            <?php
                            $event = htmlentities($model[$i]->content);
                            echo $event;
                            ?>

                        </div>
                        <a class="tog" style="cursor: pointer" data-toggle="collapse"
                           data-target="#demo<?= $model[$i]->id ?>">
                            <span data-toggle="tooltip" data-placement="right" title="<?=Yii::t('common', 'preview');?>" class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </div>


                    <p class="text-right"><?php

                        echo date('d-m-Y', strtotime($model[$i]->created_at));

                        ?>
                    </p>


                </div>
            </div>
        </div>
    </div>

<?php endfor; ?>

</div>
