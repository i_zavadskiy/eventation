<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use \yii\helpers\Url;
\yii\web\JqueryAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>




    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode('Eventation') ?></title>
    <?php $this->head() ?>

</head>
<body >
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="col-lg-12">
        <?php
        NavBar::begin([
            'brandLabel' => 'Eventation - invite your friends',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                Yii::$app->user->isGuest ? (

                   ''

                ) : (
                ['label' => Yii::t('common','Events'), 'items' => [

                    ['label' => Yii::t('common', 'My events'), 'url' => ['/events/event/my-events']],
                    ['label' => Yii::t('common', 'Confirm events'), 'url' => ['/events/event/my-confirmed']],
                    ['label' => Yii::t('common', 'Create event'), 'url' => ['/events/event/create-event']],
                ]]
                ),
                ['label' => Yii::t('common', 'Calendar'), 'url' => ['/events/calendar/show']],
                ['label' => Yii::t('common', 'Map'), 'url' => ['/events/map/show']],
                Yii::$app->user->isGuest ? (
                ['label' => Yii::t('common', 'Sign Up'), 'url' => ['#'], 'linkOptions' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'id' => 'reg'
                ]]

                ) : (
                ['label' => Yii::t('common', 'Profile'), 'url' => ['/users/profile/index']]
                ),
                Yii::$app->user->can('forAdmin')?(
                ['label' => Yii::t('common', 'Admin'), 'url' => ['/admin/admin/home']]
                ):(''),

                Yii::$app->user->isGuest ? (
                ['label' => Yii::t('common', 'Sign In'), 'url' => ['/users/auth/login'], 'linkOptions' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'id' => 'log'
                ]]

                ) : (
                ['label' => Yii::t('common', 'Sign Out'), 'url' => ['/users/auth/logout']]

                ),
            ],
        ]);
        NavBar::end();
        ?>


        <?php
        Modal::begin([
            'header' => '<h2 id="h_reg">Loading</h2>',
            //'size' => Modal::SIZE_LARGE,

            'options' => [
                'id' => 'modal',
                'class'=>'fade',
                'backdrop'=>'static',

            ]
        ]);


        Modal::end();
        ?>



    </div>
    <div class="container2">
        <?php if(Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <div align="center"> <?php echo Yii::$app->session->getFlash('success'); ?></div>
            </div>
        <?php endif; ?>
        <?php if(Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <div align="center"> <?php echo Yii::$app->session->getFlash('error'); ?></div>
            </div>
        <?php endif; ?>








    </div>

    <div class="container" id="load">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
    </div>

<div class="navbar-fixed-bottom row-fluid" align="center">
    <div class="navbar-inner">
<?php

Url::remember();
 ?>
<p>&copy; NIX Solutions <?= date('Y') ?> |    <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
        'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
        'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_SMALL
    ]); ?> |  <a href="#" id="rep3" ><?= Yii::t('common','Report about mistake')?> </a></p>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
