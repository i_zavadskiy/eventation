<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\widgets\Menu;
$this->registerCssFile('/css/admin-panel.css');
\yii\web\JqueryAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode('Eventation') ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php

NavBar::begin([
    'options'=>[
        'id'=>'admin-panel'
    ],
    'brandUrl'=>'/admin/home',
    'brandLabel' => 'Admin panel']);
echo Nav::widget([
    'items' => [

    ],
    'options' => ['class' => 'navbar-nav'],
]);
NavBar::end();

?>

<div class="row">
    <div class="col-lg-2">
<?php

//
//NavBar::begin([
//    'brandLabel' => 'Hundekauf',
//    'brandUrl' => Yii::$app->homeUrl,
//    'options' => [
//        'class' => 'navbar-default',
//    ],
//]);



echo Nav::widget([
    'options' => [
        'class' => 'nav nav-pills nav-stacked',
    ],
    'items' => [
        [
            'label' => 'Main',
            'url' => ['/admin/admin/home'],

        ],
        '</br>',
        '</br>',
        '</br>',
        [
            'label' => 'Events',
            'url' => ['/admin/admin/event-panel'],

        ],
        [
            'label' => ' ----Create',
            'url' => ['/admin/admin/create-event'],

        ],
        [
            'label' => 'Users',
            'url' => ['/admin/admin/user-panel'],

        ],
        [
            'label' => ' ----Create',
            'url' => ['/admin/create-user'],

        ],
        [
            'label' => 'Comments',
            'url' => ['/admin/admin/comment-panel'],

        ],
        [
            'label' => 'Claims',
            'url' => ['/admin/admin/claim-panel'],

        ],
        [
            'label' => 'Reports',
            'url' => ['/admin/admin/report-panel'],

        ],
        [
            'label' => 'Exit',
            'url' => ['/'],
        ],
    ],
    // set this to nav-tab to get tab-styled navigation
]);

//NavBar::end();
?>
</div>


<div class="col-lg-9">
<div class="container" id="load">
    <?= $content ?>

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

</div>
</div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
