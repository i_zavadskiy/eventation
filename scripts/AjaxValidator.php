<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 26.07.16
 * Time: 10:28
 */
namespace app\scripts;

use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

trait AjaxValidator {

    public function validateQuery($model)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        echo json_encode(ActiveForm::validate($model));
        Yii::$app->end();
    }

}