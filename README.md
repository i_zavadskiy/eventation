To set EVENTATION on your local server:


1 In your server folder. Open bash and type "**git clone https://[YOUR_USERNAME]@bitbucket.org**"

2 Check on Bitbacket branch with current update. then type "**git checkout <valid branch>**"

3 Type "**composer install**". 

4 Copy /config/db.php.dist to /config/db.php /web/.htaccess.dist to /web/.htaccess

5 Create database and configure file /config/db.php with your settings.

6 Type "**php yii migrate --migrationPath=yii/rbac/migrations/**"

7 Type "**php yii rbac/init**"

8 Type "**php yii migrate**"

9 Open acces to this folders (**/web**, **/web/assets, /runtime**)


10 Profit



To run TESTS:

1 Type "**php ./vendor/bin/codecept run**"
