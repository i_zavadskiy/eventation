<?php

use yii\db\Migration;

class m160901_091743_cteate_claim_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `claims`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `type` VARCHAR(45) NOT NULL,
          `content` TEXT NOT NULL,
          `created_at` TIMESTAMP NOT NULL,
          `updated_at` TIMESTAMP,
          `comment_id` INTEGER(11) NOT NULL,
          FOREIGN KEY (comment_id) REFERENCES comments(id)
          ON DELETE CASCADE 
        )");
    }

    public function down()
    {
        $this->dropTable('claims');
    }


}
