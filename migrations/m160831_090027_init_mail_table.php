<?php

use yii\db\Migration;

class m160831_090027_init_mail_table extends Migration
{
    public function up()
    {
        $this->execute(" 
        INSERT INTO emails (type, subject, content, html_body)
       VALUES ('confirm', 'Confirm email','Click on this link for confirm ','<p>Click on this link to confirm: </p> <a href=');
        ");
        $this->execute(" 
        INSERT INTO emails (type, subject, content, html_body)
       VALUES ('password', 'Recover password','Click on this link to continue ','<p>Click on this link to continue: </p> <a href=');
        ");
        $this->execute(" 
        INSERT INTO emails (type, subject, content, html_body)
       VALUES ('remind', 'Your event will be soon','Click on this link to continue ','<p>Click on this link to continue: </p> <a href=');
        ");
    }

    public function down()
          {$this->execute(" 
        DELETE  FROM emails;
         ");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
