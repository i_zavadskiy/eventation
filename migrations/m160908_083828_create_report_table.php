<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report`.
 */
class m160908_083828_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("CREATE TABLE `reports`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `type` VARCHAR(45) NOT NULL,
          `content` TEXT NOT NULL,
          `url` VARCHAR(100) NOT NULL,
          `created_at` TIMESTAMP NOT NULL,
          `updated_at` TIMESTAMP
        )");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('reports');
    }
}
