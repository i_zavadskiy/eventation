<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vote_event`.
 */
class m160905_144533_create_vote_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("CREATE TABLE `vote_event`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT, 
          `user_id` INTEGER(11) NOT NULL,
          `event_id` INTEGER(11) NOT NULL,
          `mark` INTEGER(11) NOT NULL,
          `created_at` TIMESTAMP NOT NULL,
          `updated_at` TIMESTAMP NOT NULL,
          FOREIGN KEY (user_id) REFERENCES users(id)
          ON DELETE CASCADE ,
          FOREIGN KEY (event_id) REFERENCES events(id)
          ON DELETE CASCADE 
        )");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('vote_event');
    }
}
