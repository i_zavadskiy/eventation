<?php

use yii\db\Migration;

class m160715_124734_init_migration extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `users`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `email` VARCHAR(45) NOT NULL,
          `password` VARCHAR(45) NOT NULL,
          `firstname` VARCHAR(20) NOT NULL,
          `lastname` VARCHAR (20) NOT NULL,
          `dob` TIMESTAMP NOT NULL,
          `role` TINYINT NOT NULL DEFAULT 0,
          `status` TINYINT NOT NULL DEFAULT 0,
          `hash` VARCHAR(45) NOT NULL,
          `img` VARCHAR(45),
           `city` VARCHAR(45),
          `created_at` TIMESTAMP DEFAULT '0000-00-00 00:00:00',
          `updated_at` TIMESTAMP DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB
          CHARACTER SET utf8 COLLATE utf8_general_ci
        ");
        $this->execute("CREATE TABLE `events`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `name` VARCHAR(45) NOT NULL,
          `content` TEXT NOT NULL,
          `date_registr` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' ,
          `date_action` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
          `author_id` INTEGER(11) NOT NULL,
          `count` INTEGER(11),
          `rating` INTEGER(11),
          `img` VARCHAR(255),
          `lat` DOUBLE, 
          `lng` DOUBLE, 
          `created_at` TIMESTAMP NOT NULL,
          `updated_at` TIMESTAMP
          ) ENGINE=InnoDB
          CHARACTER SET utf8 COLLATE utf8_general_ci
        ");
        $this->execute("CREATE TABLE `emails`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `type` VARCHAR(20) NOT NULL,
          `subject` VARCHAR(20) NOT NULL,
          `content` TEXT NOT NULL,
          `html_body` TEXT NOT NULL,
          `created_at` TIMESTAMP NOT NULL,
          `updated_at` TIMESTAMP
          ) ENGINE=InnoDB
          CHARACTER SET utf8 COLLATE utf8_general_ci
        ");
        $this->execute("CREATE TABLE `users_events`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT, 
          `user_id` INTEGER(11) NOT NULL,
          `event_id` INTEGER(11) NOT NULL,
          `date_confirm` TIMESTAMP NOT NULL,
          FOREIGN KEY (user_id) REFERENCES users(id)
          ON DELETE CASCADE ,
          FOREIGN KEY (event_id) REFERENCES events(id)
          ON DELETE CASCADE 
          ) ENGINE=InnoDB
          CHARACTER SET utf8 COLLATE utf8_general_ci;
          
        ");
        $this->execute("CREATE TABLE `comments`(
          `id` INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
          `relative` INTEGER(11),
          `content` TEXT NOT NULL,
          `html_body` TEXT NOT NULL,
          `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
          `updated_at` TIMESTAMP DEFAULT '0000-00-00 00:00:00',
          `user_id` INTEGER(11) NOT NULL,
          `event_id` INTEGER(11) NOT NULL,
          FOREIGN KEY (user_id) REFERENCES users(id)
          ON DELETE CASCADE ,
          FOREIGN KEY (event_id) REFERENCES events(id)
          ON DELETE CASCADE,
          FOREIGN KEY (relative) REFERENCES comments(id) 
          ON DELETE CASCADE
          ) ENGINE=InnoDB
          CHARACTER SET utf8 COLLATE utf8_general_ci;
        ");
    }

    public function down()
    {
        //echo "m160715_092420_init_migration cannot be reverted.\n";
        $this->dropTable('users_events');
        $this->dropTable('comments');
        $this->dropTable('emails');


        $this->dropTable('events');
        $this->dropTable('users');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
