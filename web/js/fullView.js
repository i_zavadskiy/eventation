/**
 * Created by zavadskyi on 10.08.16.
 */
$(document).ready(function () {


    $('#load').on('click', 'a.tog', function () {

        if ($(this).children('span').hasClass('glyphicon glyphicon-eye-open')) {
            $(this).children('span').removeClass("glyphicon glyphicon-eye-open").addClass('glyphicon glyphicon-eye-close')
        } else {
            $(this).children('span').removeClass("glyphicon glyphicon-eye-close").addClass('glyphicon glyphicon-eye-open')
        }

    });


});