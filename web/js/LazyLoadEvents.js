$(document).ready(function () {
    var i = 0;
    var prev;

    $(document).scroll(function () {

        if ($(window).scrollTop() > $(document).height() - $(window).height() - 200) {


            if (i != null) {
                i++;
                $.ajax({
                    url: "/site/lazy-index?page=" + i,
                    type: "POST",

                    success: function (response) {
                        if (prev != response) {
                            $('.out').append(response);
                        } else {
                            i = null;
                        }
                        prev = response;
                    },
                    error: function () {

                    },
                    complete: function () {
                    }
                });
            }

        }
    });
});
