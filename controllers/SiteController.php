<?php

namespace app\controllers;

use app\models\ReportModel;
use app\modules\events\models\Events;
use dosamigos\qrcode\formats\MailTo;
use dosamigos\qrcode\QrCode;
use  yii\web\Cookie;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use app\scripts\AjaxValidator;

class SiteController extends Controller
{

    use AjaxValidator;
    private $_startEvents = 4;
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()

    {


        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'test',
            'value' => 'testValue'
        ]));





        $model = Events::find()->orderBy(['count' => SORT_DESC])->limit(6)->all();

        $event = Events::find()->orderBy(['created_at' => SORT_DESC])->limit($this->_startEvents)->all();

        $this->layout = '/main2';
        return $this->render('index',['model'=>$model,'event'=>$event]);



    }

    public function actionLazyIndex()

    {

        $model = Events::find()->orderBy(['created_at' => SORT_DESC]);

        $countQuery = clone $model;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 5,
        ]);

        $models = $model->offset($pages->offset+$this->_startEvents)
            ->limit($pages->limit)
            ->all();

        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
            return $this->render('events-lazy', [
                'model' => $models, 'pages' => $pages,
            ]);
    }

    public function actionReport(){


        $model = new ReportModel();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            }else{
                $model->url = Yii::$app->request->post('ReportModel')['url'];
                $model->save();
                Yii::$app->session->setFlash('success', Yii::t('common','Thank you! We will check this soon'));
                return $this->redirect('/');
            }
        }else {
         return $this->renderAjax('report', ['model' => $model, 'url' => Url::previous()]);

        }
    }

    public function actionQrcode() {
        return QrCode::png('http://10.10.54.150'.Url::previous());
    }
}
