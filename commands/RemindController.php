<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 09.08.16
 * Time: 11:19
 */

namespace app\commands;


use app\models\RemindModel;
use yii\console\Controller;

class RemindController extends Controller
{

    public function actionIndex($message = 'remind')
    {
        echo $message;
    }


    public function actionCheck(){

        $model = new RemindModel();
        $events = $model ->getEvents();
        foreach($events as $value){
            $model->remind($value);
        }

    }
}