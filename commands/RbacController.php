<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 21.07.16
 * Time: 10:33
 */

namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{

    public function actionInit()
    {

        $role = Yii::$app->authManager->createRole('admin');
        $permission = Yii::$app->authManager->createPermission('forAdmin');
        $role->description = 'Admin';
        Yii::$app->authManager->add($role);
        Yii::$app->authManager->add($permission);
        Yii::$app->authManager->addChild($role, $permission);

        $role = Yii::$app->authManager->createRole('user');
        $permission = Yii::$app->authManager->createPermission('forUser');
        $role->description = 'User';
        Yii::$app->authManager->add($role);
        Yii::$app->authManager->add($permission);
        Yii::$app->authManager->addChild($role, $permission);
    }

}