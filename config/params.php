<?php

return [
    'adminEmail' => 'admin@example.com',
    'uploadThumbUser' => 'uploads/thumbnail/',
    'uploadMediumUser' => 'uploads/medium/',
    'uploadThumbEvent' => 'uploads/event/thumbnail/',
    'uploadMediumEvent' => 'uploads/event/medium/',
];
