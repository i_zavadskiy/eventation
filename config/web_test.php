<?php


$params = require(__DIR__.'/params.php');

$config = [
    'id' => 'basic',


    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage' => 'en',
    'language' => 'en',
    'components' => [

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'EGCy83r2tNVX9kW06WZ8JFyCKnBZqlPM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\users\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['/'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'test.site.event@gmail.com',
                'password' => 'eventation',
                'port' => '587',
                'encryption' => 'tls',],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__.'/db_test.php'),


        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth',
                    'scope'=>['email','public_profile'],
                    'attributeNames'=>['birthday','first_name','last_name','email'],
                    'clientId' => '157350654702961',
                    'clientSecret' => 'a7c67ae7ffcc4b3d6ef6c2676ac49060',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'codemix\localeurls\UrlManager',
            'enableDefaultLanguageUrlCode' => true,

            'rules' => [
                'event/<action>'=>'events/event/<action>',
                'admin/<action>'=>'admin/admin/<action>',
                'admin/<action>/<id:\d+>'=>'admin/admin/<action>',
                'events/<action>/<id:\d+>'=>'events/event/<action>',
                'events/<action>/<r:\d+>/<id:\d+>'=>'events/event/<action>',
                'events/comment/<action>/<id:\d+>'=>'events/comment/<action>',
                'events/calendar/<action>/<d:\d+>/<m:\d+>/<y:\d+>'=>'events/calendar/<action>',
                'users/<action>/<id:\d+>'=>'users/profile/<action>',
            ],
            'languages' => ['ru', 'en'],
        ],

        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],


    ],
    'modules' => [
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
        'events' => [
            'class' => 'app\modules\events\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'params' => $params,
];

if(YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
