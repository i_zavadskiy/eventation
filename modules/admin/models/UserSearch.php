<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 22.08.16
 * Time: 11:01
 */

namespace app\modules\admin\models;


use app\modules\users\models\User;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['firstname','lastname', 'created_at'], 'safe'],
        ];
    }

    public static function tableName()
    {
        return 'users';
    }

    public function search($params)
    {
        $query = $this::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 7,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }

}