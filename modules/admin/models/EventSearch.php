<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 22.08.16
 * Time: 10:32
 */

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class EventSearch extends \app\modules\events\models\Events
{

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'created_at'], 'safe'],
        ];
    }

    public static function tableName()
    {
        return 'events';
    }

    public function search($params)
    {
        $query = $this::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 7,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'name' => [
                        'asc' => ['name' => SORT_ASC],
                        'desc' => ['name' => SORT_DESC],

                        'default' => SORT_ASC
                    ],
                    'created_at',
                ]
            ]
        ]);

        
        if(!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'created_at', $this->created_at]);

        return $dataProvider;
    }

}