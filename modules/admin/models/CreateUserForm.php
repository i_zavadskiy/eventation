<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 22.08.16
 * Time: 11:16
 */

namespace app\modules\admin\models;


use yii\db\ActiveRecord;
use yii;
use yii\behaviors\TimestampBehavior;

class CreateUserForm extends ActiveRecord
{

    public $password2;
    public $imageFile;

    public function rules()
    {

        return [
            [['email', 'password', 'firstname', 'lastname', 'city'], 'trim'],
            [['email', 'password', 'password2', 'firstname', 'lastname', 'dob','role'], 'required'],
            ['firstname', 'string', 'min' => 3, 'max' => 45],
            ['lastname', 'string', 'min' => 3, 'max' => 45],
            ['password', 'string', 'min' => 6, 'max' => 45],
            ['email', 'email'],
            ['email', 'unique', 'message' => 'Email already exists!'],
            ['password2', 'compare', 'compareAttribute' => 'password', 'operator' => '=='],
            ['dob', 'date', 'format' => 'yyyy-MM-dd'],
            ['dob', 'dateValidate'],
        ];
    }


    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {

        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() {
                    return date('Y-m-d');
                },
            ],
        ];
    }


    public function dateValidate($attribute)
    {
        $str1 = date("Ymd");
        $str2 = date("Ymd", strtotime($this[$attribute]));
        if($str2 > $str1) {
            $this->addError('dob', 'Incorrect date!');
        }
        return true;
    }

    public function beforeSave($insert)
    {
        $this->hash = md5($this->password2.time());
        $this->password = md5($this->password);
        $this->status = 1;
        return parent::beforeSave($insert);
    }
    
}