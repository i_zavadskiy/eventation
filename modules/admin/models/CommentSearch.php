<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 22.08.16
 * Time: 15:47
 */

namespace app\modules\admin\models;


use app\modules\events\models\Comments;
use yii\data\ActiveDataProvider;

class CommentSearch extends Comments
{

    public function rules()
{
    return [
        [['id'], 'integer'],
        [['content', 'created_at'], 'safe'],
    ];
}

    public static function tableName()
{
    return 'comments';
}

    public function search($params)
{
    $query = $this::find();

    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 7,
        ],
    ]);

    if(!($this->load($params) && $this->validate())) {
        return $dataProvider;
    }

    $query->andFilterWhere(['id' => $this->id]);
    $query->andFilterWhere(['like', 'content', $this->content])
        ->andFilterWhere(['like', 'created_at', $this->created_at]);

    return $dataProvider;
}

}