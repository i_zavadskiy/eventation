<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 01.09.16
 * Time: 16:23
 */

namespace app\modules\admin\models;


use app\models\ReportModel;
use yii\data\ActiveDataProvider;

class ReportSearch extends ReportModel
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['content', 'created_at', 'type'], 'safe'],
        ];
    }

    public static function tableName()
    {
        return 'reports';
    }

    public function search($params)
    {
        $query = $this::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 7,
            ],
        ]);

        if(!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'url', $this->url]);


        return $dataProvider;
    }

}
