<?php
namespace app\modules\admin\controllers;

use app\models\ReportModel;
use app\modules\admin\models\ClaimSearch;
use app\modules\admin\models\CommentSearch;
use app\modules\admin\models\CreateUserForm;
use app\modules\admin\models\EditUserForm;
use app\modules\admin\models\ReportSearch;
use app\modules\admin\models\UserSearch;
use app\modules\events\models\Claims;
use app\modules\events\models\Comments;
use app\modules\events\models\Events;
use app\modules\events\models\UsersEvents;
use app\modules\users\models\User;
use Yii;
use app\modules\admin\models\EventSearch;
use yii\filters\AccessControl;
use app\scripts\AjaxValidator;
use app\models\ImageUpload;

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.08.16
 * Time: 15:10
 */
class AdminController extends \yii\web\Controller
{

    public $layout = '/admin';
    use AjaxValidator;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];

    }

    public function actionHome(){
        return $this->render('home');
    }

    public function actionEventPanel()
    {

        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        

        return $this->render('event-panel', ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel]);

    }




    public function actionCreateEvent()
    {

        $model = new Events();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } else {
                if($_FILES['Events']['name']['imageFile']!='') {
                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                        'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);

                    $model->img = $imageModel->file_name;
                }
                $model->setID();
                $model->save();

                return $this->redirect('/admin/admin/home');
            }

        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionEditEvent()
    {
        $model = Events::find()->where(["id" => Yii::$app->request->get('id')])->one();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } else {
                if($_FILES['Events']['name']['imageFile']!='') {
                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                        'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);

                    $model->img = $imageModel->file_name;
                }
                $model->update();

                return $this->redirect('/admin/admin/home');

            }
        }

        return $this->render('create', ['model' => $model]);
    }

//    public function actionDeleteEvent()
//    {
//
//        $event = Events::findOne(Yii::$app->request->get('id'));
//
//            $userEvent = UsersEvents::find()->where(['event_id' => $event->id])->all();
//            $comments = Comments::find()->where(['event_id' => $event->id])->all();
//            foreach($comments as $value) {
//                $value->delete();
//            }
//            foreach($userEvent as $value) {
//                $value->delete();
//            }
//            $event->delete();
//
//        return $this->redirect('/admin/admin/event-panel');
//    }

    public function actionUserPanel()
    {

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('user-panel', ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel]);

    }

    public function actionCommentPanel()
    {

        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('comment-panel', ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel]);

    }

    public function actionCreateUser()
    {

        $model = new CreateUserForm();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } else {

                if($_FILES['CreateUserForm']['name']['imageFile']!='') {
                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/',
                        'upload_url' => 'uploads/','param_name' => 'CreateUserForm']);
                    $model->img = $imageModel->file_name;
                }
                $model->save();


                return $this->redirect('/admin/admin/user-panel');
            }
        }

        return $this->render('create-user', ['model' => $model]);
    }

    public function actionEditUser()
    {

        $model = EditUserForm::findOne(Yii::$app->request->get('id'));
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } else {

                if($_FILES['EditUserForm']['name']['imageFile']!='') {
                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/',
                        'upload_url' => 'uploads/','param_name' => 'EditUserForm']);
                    $model->img = $imageModel->file_name;
                }
                if($model->save()) {


                    return $this->redirect('/admin/admin/user-panel');
                }
            }
        }

        return $this->render('edit-user', ['model' => $model]);
    }

    public function actionDeleteUser()
    {
        if(Yii::$app->request->get('id') == Yii::$app->user->identity->getId()) {
            return $this->redirect('/admin/admin/user-panel');
        }
        $user = User::findOne(Yii::$app->request->get('id'));
        $userEvent = UsersEvents::find()->where(['user_id' => $user->id])->all();
        $comments = Comments::find()->where(['user_id' => $user->id])->all();
        foreach($comments as $value) {
            $value->delete();
        }
        foreach($userEvent as $value) {
            $value->delete();
        }
        $user->delete();
        return $this->redirect('/admin/admin/user-panel');
    }


    public function actionDeleteComment()
    {
        $comment = Comments::findOne(Yii::$app->request->get('id'));
        $claims = Claims::find()->where(['comment_id'=>Yii::$app->request->get('id')])->all();
        foreach($claims as $value){
            $value->delete();
        }
        $comment->delete();
        return $this->redirect('/admin/admin/comment-panel');
    }

    public function actionEditComment()
    {
        $comment = Comments::findOne(Yii::$app->request->get('id'));
        if(Yii::$app->request->isPost && $comment->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($comment);
            } else {
            }
            $comment->update();
            return $this->redirect('/admin/admin/comment-panel');
            }
        return $this->render('edit-comment',['model'=>$comment]);
    }


    public function actionClaimPanel()
    {

        $searchModel = new ClaimSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('claim-panel', ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel]);

    }
    public function actionDeleteClaim()
    {

        $claim = Claims::findOne(Yii::$app->request->get('id'));
        $claim->delete();
        return $this->redirect('/admin/admin/claim-panel');
    }
    public function actionCheckClaim(){

        $comment = Comments::findOne(Yii::$app->request->get('id'));
        $user = User::findOne($comment->user_id);
        return $this->render('check-claim',['model'=>$comment,'user'=>$user]);
    }
    public function actionReportPanel()
    {

        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('report-panel', ['dataProvider' => $dataProvider,
            'filterModel' => $searchModel]);

    }
    public function actionDeleteReport()
    {

        $claim = ReportModel::findOne(Yii::$app->request->get('id'));
        $claim->delete();
        return $this->redirect('/admin/admin/report-panel');
    }
}