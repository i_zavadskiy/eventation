<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.08.16
 * Time: 15:19
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use \app\modules\users\models\User;

?>
<h1>Event management</h1>
<div align="right"><a href="/admin/admin/create-event" class="btn btn-primary">Create</a></div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'label' => 'Image',
            'format' => 'raw',
            'value' => function($data) {
                if($data['img'] != null) {
                    return '<img class="media-object" src="http://10.10.54.150/uploads/event/thumbnail/'.$data['img'].'" style="max-width: 60px" alt="ava">';
                }else{
                    return '<img class="media-object" src="http://10.10.54.150/images/img.jpg" style="max-width: 60px" alt="ava">';
                }
            },
        ],
        'name',
        'created_at:datetime',
        [
            'label' => 'Author_id',
            'format' => 'raw',
            'value' => function($data) {

                $user = User::findOne($data['author_id']);
                return '<a href="/users/profile/'.$data['author_id'].'">'.$user->firstname.' '.$user->lastname.'</a>';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {edit} {delete}',
            'buttons' => [
                'show' => function($url, $model) {
                    $url = '/events/show/'.$model['id'];
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url);
                },
                'edit' => function($url, $model) {
                    $url = '/admin/edit-event/'.$model['id'];
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url);
                },
                'delete' => function($url, $model) {
                    $url = 'admin/admin/delete-event/'.$model['id'];
                    return '<a href="'.$url.'" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ],
        ],
        // ...
    ],

]) ?>
