<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>


<?php $form = ActiveForm::begin([
    'id' => 'edit-comment-form',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-4\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]); ?>


<?= $form->field($model, 'content')->textarea(['rows' => 10, 'style' => ['resize' => 'none']])->label('Enter new comment') ?>




    <div class="col-lg-offset-5 col-lg-11">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary loading', 'name' => 'login-button', 'id' => 'edit_b']) ?>
    </div>


    <?php ActiveForm::end(); ?>

