<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<h1>Claim management</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'type',
        'content',
        'created_at:datetime',
        'comment_id',
        // ...
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {delete}',
            'buttons' => [
                'show' => function($url,$model) {
                    $url = '/admin/check-claim/'.$model['comment_id'];
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url);
                },
                'delete' => function($url,$model) {
                    $url = '/admin/delete-claim/'.$model['id'];
                    return '<a href="'.$url.'" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ],
        ],
    ],


]) ?>
