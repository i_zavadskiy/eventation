<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.08.16
 * Time: 15:19
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<h1>User management</h1>

<div align="right"><a href="/admin/create-user" class="btn btn-primary">Create</a></div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        [
            'label' => 'Image',
            'format' => 'raw',
            'value' => function($data) {
                if($data['img'] != null) {
                    return '<img class="media-object" src="http://10.10.54.150/uploads/thumbnail/'.$data['img'].'" style="max-width: 60px" alt="ava">';
                }else{
                    return '<img class="media-object" src="http://10.10.54.150/images/2.png" style="max-width: 60px" alt="ava">';
                }
            },
        ],
        'firstname',
        'lastname',
        'role',
        'created_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {edit} {delete}',
            'buttons' => [
                'show' => function($url, $model) {
                    $url = '/users/profile/'.$model['id'];
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url);
                },
                'edit' => function($url, $model) {
                    $url = '/admin/edit-user/'.$model['id'];
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url);
                },
                'delete' => function($url, $model) {
                    $url = '/admin/delete-user/'.$model['id'];
                    return '<a href="'.$url.'" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ],
        ],
        // ...
    ],

]) ?>
