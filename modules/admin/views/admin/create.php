<?php
//margin-top: 40px; margin-left:135px
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 28.07.16
 * Time: 11:10
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use dosamigos\datetimepicker\DateTimePicker;


$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBCepoa6V5kvv9oRHtMkslf3hPILT5a45Y&callback=initMap');
?>
<div class="col-lg-8 col-lg-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 align="center">Create your best event!</h3></div>
        <div class="panel-body">

        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'data-form',
            'enableAjaxValidation' => true,
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-4 col-lg-offset-4\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <div class="output" id="out" style="margin: 10px 10px 10px 10px" align="center"></div>
        <div class="row">
            <label class="col-lg-3 control-label" id="leb" for="map">Choose place*</label>
            <div align="center" class="col-lg-8" id="map" style="height: 300px; width: 475px; margin-left:15px"></div>

            <div class="col-lg-11">
                <p style="color: firebrick" align="center" id="error"></p>
            </div>
        </div>
        <?= $form->field($model, 'name')->textInput()->label('Event name*') ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 10, 'style' => ['resize' => 'none']])->label('Description*') ?>

        <?= $form->field($model, 'date_action')->widget(DateTimePicker::className(), [
            'language' => 'en',
            'size' => 'ms',
            'template' => '{input}',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'inline' => false,
            'clientOptions' => [
                'startView' => 3,
                'minView' => 0,
                'maxView' => 4,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
                'todayBtn' => true
            ]
        ]);?>


        <?= $form->field($model, 'date_registr')->widget(DateTimePicker::className(), [
            'language' => 'en',
            'size' => 'ms',
            'template' => '{input}',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'inline' => false,
            'clientOptions' => [
                'startView' => 3,
                'minView' => 2,
                'maxView' => 4,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
                'todayBtn' => true
            ]
        ]);?>


        <script>
            var img = '<?= $model->getFull(); ?>';

            if (img != "") {
                document.getElementById('out').innerHTML = "<img src=\" <?= $model->getFull() ?>  \" class=\"img-rounded\" style=\"max-width: 200px\"alt=\"p\">";

            }

            function foo(e) {
                var file = e.target.files[0];
                var reader = new FileReader();
                if (file.type == 'image/jpeg' || file.type == 'image/png') {
                    reader.readAsDataURL(file);
                    reader.onload = (function (file, data) {
                        $('.output').html("<img src=\" " + file.target.result + " \" class=\"img-rounded\" style=\"max-width: 200px\"alt=\"p\">");
                    });
                }
            }
            document.getElementById('events-imagefile').addEventListener('change', foo, false);
        </script>


        <script type="text/javascript">
            var map;
            var markers;
            var marker2;
            var lat;
            var lng;

            function initMap() {

                var mapOptions = {
                    center: {lat: 50, lng: 36},
                    zoom: 7,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false
                };

                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                map.addListener('click', function (e) {
                    if(marker2 != null) {
                        marker2.setMap(null);
                    }
                    $('#error').html('');
                    $('#leb').css('color', 'black');
                    if (markers == null) {
                        placeMarkerAndPanTo(e.latLng, map);
                    } else {
                        markers.setMap(null);
                        placeMarkerAndPanTo(e.latLng, map);
                    }

                });

                if ('<?= $model->lat?>' != '') {
                   marker2 = new google.maps.Marker({
                        position: {lat: +'<?= $model->lat?>', lng: +'<?= $model->lng?>'},
                        map: map,
                        title: 'Event'
                    });

                    var pos2 = {
                        lat: +'<?= $model->lat?>',
                        lng: +'<?= $model->lng?>'
                    };
                    map.setCenter(pos2);

                }

            }

            if ('<?= $model->lat?>' == '') {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    map.setCenter(pos);
                });
            }
            if(+'<?= $model->lat?>'!=0 && lat !=0){
                marker2.setMap(null);
            }

            function placeMarkerAndPanTo(latLng, map) {

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: "Best event",
                    draggable: true,
                    animation: google.maps.Animation.DROP
                });
                lat = latLng.lat();
                lng = latLng.lng();
                $('#events-lat').val(lat);
                $('#events-lng').val(lng);

                markers = marker;
                map.panTo(latLng);
            }


        </script>
        <div hidden>
            <?= $form->field($model, 'lat')->textInput(['readonly' => true])->label('lat*') ?>
            <?= $form->field($model, 'lng')->textInput(['readonly' => true])->label('lng*') ?>
        </div>

        <div class=" form-group">
            <div class="col-lg-offset-5 col-lg-11">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'event_b']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
<script>

if(+'<?= $model->lat ?>' == 0) {
    document.getElementById('event_b').addEventListener('click', function () {
        if (lat == null && lng == null) {
            $('#error').html('Choose your event place');
            $('#leb').css('color', 'firebrick');
        } else {
            $('#error').html('');
        }


    });
}
</script>