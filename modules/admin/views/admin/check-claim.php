<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 01.09.16
 * Time: 17:08
 */

use app\modules\users\models\User;
use Yii;
use yii\helpers\Url;

?>

<div class="panel panel-default">
    <div class="panel-body">

        <div class="media">
            <a class="pull-left" href="<?= Url::to(["/users/profile/".$user->id]) ?>">
                <?php if($user->img != null): ?>
                    <img class="media-object" src="<?= $user->getThumb() ?>" style="max-width: 100px"
                         alt="ava">
                <?php endif; ?>
                <?php if($user->img == null): ?>
                    <img class="media-object" src="http://10.10.54.150/images/2.png"
                         style="max-width: 100px" alt="ava">
                <?php endif; ?>

            </a>
            <div class="media-body">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <h4 class="media-heading"><a
                                href="<?= Url::to(["/users/profile/".$user->id]) ?>"><?= $user->firstname ?> <?= $user->lastname ?></a>
                        </h4>
                    </div>
                </div>

                <div class="test">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <?php
                            $comment = htmlentities($model['content']);
                            echo $comment;
                            ?>
                        </div>


                    </div>
                    <p class="text-right">
                        <?php
                        echo $model['created_at']

                        ?>


                    </p>

                </div>
                <div class="hidden" id="form1">
                </div>
            </div>


        </div>

    </div>

</div>
<div class="row">

<div class="col-lg-1"><a href="/admin/admin/claim-panel" class="btn btn-primary">Back</a></div>
    <div class="col-lg-2"><a href="/admin/edit-comment/<?=$model['id']?>" class="btn btn-primary">Edit comment</a></div>

    <div class="col-lg-2"><a href="/admin/delete-comment/<?=$model['id']?>" class="btn btn-primary">Delete comment</a></div>

    <div class="col-lg-2"><a href="/admin/delete-user/<?=$user['id']?>" class="btn btn-primary">Delete user</a></div>






</div>