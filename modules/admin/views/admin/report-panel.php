<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<h1>Report management</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'type',
        'content',
        'url',
        'created_at:datetime',
        // ...
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {delete}',
            'buttons' => [
                'show' => function($url,$model) {

                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $model['url']);
                },
                'delete' => function($url,$model) {
                    $url = '/admin/delete-report/'.$model['id'];
                    return '<a href="'.$url.'" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ],
        ],
    ],


]) ?>
