<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

?>


<?php $form = ActiveForm::begin([
    'id' => 'create-user-form',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-4\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]); ?>

<?= $form->field($model, 'email')->textInput()->hint('Enter, your email')->label('Email*') ?>

<?= $form->field($model, 'password')->passwordInput()->hint('Enter, your password')->label('Password*') ?>
<?= $form->field($model, 'password2')->passwordInput()->hint('Enter, your password')->label('Password again*') ?>

<?= $form->field($model, 'firstname')->textInput()->hint('Enter, your first name')->label('First name*') ?>

<?= $form->field($model, 'lastname')->textInput()->hint('Enter, your last name')->label('Last name*') ?>

<?=
$form->field($model, 'role')
    ->dropDownList([
        '1' => 'User',
        '2' => 'Admin',
    ])->label('Role')
?>

<?= $form->field($model, 'city')->textInput()->hint('Enter, your sity')->label('City') ?>

<?= $form->field($model, 'dob')->widget(DatePicker::className(),
    array(

        'model' => $model,
        'dateFormat' => 'y-M-d',

        'options' => array(

            'class' => 'form-control',
            'showAnim' => 'fadeIn',
            'showButtonPanel' => 'true',
            'changeMonth' => 'true',
            'changeYear' => 'true',
        ),
        'clientOptions' => array(
            'autoclose' => true,
            'showAnim' => 'fadeIn',
            'showButtonPanel' => 'true',
            'changeMonth' => 'true',
            'changeYear' => 'true',
            'yearRange' => '-70:-0',
            'maxDate' => date('Y-m-d'),
        )

    ))->label('Dob*') ?>
<?= $form->field($model,'imageFile')->fileInput()?>

<div class="output" align="center"></div>

<script>
    function foo(e){
        var file =  e.target.files[0];
        var reader = new FileReader();
        if(file.type == 'image/jpeg' || file.type == 'image/png') {
            reader.readAsDataURL(file);
            reader.onload = (function (file, data) {
                console.log();
                $('.output').html("<img src=\" " + file.target.result + " \" class=\"img-rounded\" style=\"max-width: 200px\"alt=\"p\">");
            });
        }
    }
    document.getElementById('createuserform-imagefile').addEventListener('change', foo, false);
</script>


<div class="form-group">

    <div class="col-lg-offset-5 col-lg-11">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary loading', 'name' => 'login-button', 'id' => 'create_b']) ?>
    </div>


    <?php ActiveForm::end(); ?>

