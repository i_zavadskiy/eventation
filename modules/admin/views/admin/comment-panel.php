<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.08.16
 * Time: 15:19
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<h1>Comment management</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'content',
        'created_at:datetime',
        [
            'label' => 'Event_id',
            'format' => 'raw',
            'value' => function($data) {
               return $data['event_id'];
            },
        ],
        [
            'label' => 'Author_id',
            'format' => 'raw',
            'value' => function($data) {
                return '<a href="/users/profile/'.$data['user_id'].'">'.$data['user_id'].'</a>';
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {edit} {delete}',
            'buttons' => [
                'show' => function($url, $model) {
                    $url = '/events/show/'.$model['event_id'];
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url);
                },
                'edit' => function($url, $model) {
                    $url = '/admin/edit-comment/'.$model['id'];
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url);
                },
                'delete' => function($url, $model) {
                    $url = '/admin/delete-comment/'.$model['id'];
                    return '<a href="'.$url.'" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ],
        ],
        // ...
    ],

]) ?>
