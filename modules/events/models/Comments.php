<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 01.08.16
 * Time: 11:54
 */

namespace app\modules\events\models;

use yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class Comments extends ActiveRecord
{


    public function rules()
    {

        return [
            [['content',], 'trim'],
            ['content', 'required','message' => Yii::t('common','Required field!')],

        ];
    }

    public function getRel(){
       return $this->hasMany(Comments::className(), ['relative' => 'id'])->from('comments rel');
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],

                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    public function getEvents()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);

    }


    public function setID()
    {

        $this->user_id = Yii::$app->user->identity->getId();

    }
    
    public function setPostId($id)
    {
        $this->event_id = $id;
    }

    public function setRelative($id){
        $this->relative = $id;
    }

}