<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 28.07.16
 * Time: 11:57
 */
namespace app\modules\events\models;

use yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use app\modules\users\models\User;

class Events extends ActiveRecord
{

    public $imageFile;
    public $coordX;
    public $coordY;
    public $height;
    public $weight;
    public $per;

    public function rules()
    {

        return [
            [['name', 'content'], 'trim'],
            [['name', 'content', 'date_registr', 'date_action', 'lat', 'lng'], 'required', 'message' => Yii::t('common', 'Field cannot be blank')],
            ['name', 'string', 'min' => 3, 'max' => 40, 'tooShort' => Yii::t('common', 'Name is too short'), 'tooLong' => Yii::t('common', 'Name is too long')],
            ['content', 'string', 'tooShort' => Yii::t('common', 'Description is too short')],
            [['date_action'], 'date', 'format' => 'yyyy-MM-dd H:i:s',],
            [['date_registr' ], 'date', 'format' => 'yyyy-MM-dd H:i:s', 'message' => Yii::t('common', 'Incorrect date')],
            [['date_registr', 'date_action'], 'dateValidate', 'message' => Yii::t('common', 'Incorrect date')],
            [['lat', 'lng'], 'double'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    public function dateValidate($attribute)
    {

        $str1 = date("YmdHis");
        $str2 = date("YmdHis", strtotime($this[$attribute]));
        if($str2 < $str1) {
            $this->addError($attribute, Yii::t('common', 'Incorrect date!'));
        }
        return true;
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('users_events', ['event_id' => 'id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['event_id' => 'id']);

    }

    public function getAuthEvents($id)
    {

        return $this->find()->where(['author_id' => $id])->all();
    }

    public function getFull()
    {
        if(isset($this->img)) {
            return "http://10.10.54.150/uploads/event/".$this->img;
        }else{
            return false;
        }
    }
    public function getMedium()
    {
        if(isset($this->img)) {
            return "http://10.10.54.150/uploads/event/medium/".$this->img;
        }else{
            return false;
        }
    }

    public function getThumb()
    {
        if(isset($this->img)) {
            return "http://10.10.54.150/uploads/event/thumbnail/" . $this->img;
        }else{
            return false;
        }
    }


    public function setName($name)
    {
        $this->img = $name;
    }


    public function setID()
    {
        $this->author_id = Yii::$app->user->identity->getId();
        $this->rating = 0;
    }



    public function afterSave($insert, $changedAttributes)
    {

        return parent::afterSave($insert, $changedAttributes);
    }

}