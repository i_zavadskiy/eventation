<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 12.08.16
 * Time: 11:40
 */

namespace app\modules\events\models;


use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

class Calendar extends ActiveRecord
{

    public $month;
    public $year;

    public function rules()
    {

        return [
            [['month', 'year'], 'required',  'message'=>'empty'],

        ];
    }

    public function getDate($event)
    {
        return (new \DateTime($event['date_action']))->format("n-j");
    }
    public function getHour($event)
    {
        return (new \DateTime($event['date_action']))->format("H");
    }

    public function dateValidate($y,$m){
        if($y == null && $m == null){
            return true;
        }
        if($m<0 || $m>12){
            return false;
        }
        if($y<2016 || $y>2018){
            return false;
        }

        return true;
    }

    public function createMonth($events, $startCalendar)
    {

        foreach($events as $value) {
            $monthEvents[$this->getDate($value)][] = $value;
        }

        $events2 = [];
        $thisDay = new \DateTime($startCalendar);
        for($i = 1; $i <= 42; $i++) {
            $events2[$thisDay->format("n-j")] = isset($monthEvents[$thisDay->format("n-j")]) ? $monthEvents[$thisDay->format("n-j")] : null;
            $thisDay->modify('next day');
        }
        return $events2;
    }

    public function createDay($events)
    {
        foreach($events as $value) {
            $dayEvents[$this->getHour($value)][] = $value;
        }
        $events2 = [];
        for($i = 0; $i <= 23; $i++) {
            $a = ($i>9)?$i:'0'.$i;
            $events2[$a] = isset($dayEvents[$a]) ? $dayEvents[$a]: null;
        }
        return $events2;
    }


}