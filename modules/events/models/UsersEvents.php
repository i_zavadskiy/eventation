<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 04.08.16
 * Time: 12:11
 */

namespace app\modules\events\models;


use app\modules\users\models\User;
use yii\db\ActiveRecord;

class UsersEvents extends ActiveRecord
{

    public static function tableName()
    {

        return 'users_events';
    }

    public static function canGo($userId, $eventId)
    {
        $actions = self::find()->where(['user_id' => $userId, 'event_id' => $eventId])->all();
        if($actions != null) {
            return false;
        }
        return true;
    }

    public function whoGo($id)
    {
        return User::find()->innerJoin('users_events', '`users`.`id` = `users_events`.`user_id`')->where(['`users_events`.`event_id`' => $id])->all();
    }

    public function whereGo($id)
    {
        return Events::find()->innerJoin('users_events', '`events`.`id` = `users_events`.`event_id`')->where(['`users_events`.`user_id`' => $id])->all();
    }


}