<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 01.09.16
 * Time: 12:34
 */

namespace app\modules\events\models;


use yii\db\ActiveRecord;

class Claims extends ActiveRecord
{

    public function rules()
    {

        return [
            ['content', 'trim'],
            ['content', 'required', 'message' => 'Field cannot be blank!'],
        ];
    }

    


}