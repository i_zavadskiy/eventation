<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 06.09.16
 * Time: 10:36
 */

namespace app\modules\events\models;


use yii\db\ActiveRecord;

class Vote extends ActiveRecord
{

    public static function tableName()
    {
        return 'vote_event';
    }


    public function count($event){
        $event = Events::findOne($event);
        $marks = Vote::find()->where(['event_id'=>$event])->all();
        $total = 0;
        foreach($marks as $value){
            $total+=$value['mark'];
        }
        $event->rating = ($total/count($marks));
        $event->save(false);
    }

    public static function canVote($userId, $eventId){
        if($userId==null){
            return false;
        }
        $actions = self::find()->where(['user_id' => $userId, 'event_id' => $eventId])->all();
        if($actions != null) {
            return false;
        }
        return true;
    }
}