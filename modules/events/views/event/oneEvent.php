<?php


use yii\helpers\Html;
use yii\helpers\Url;
use \yii\widgets\ActiveForm;
use \app\modules\events\models\UsersEvents;
use app\modules\users\models\User;
use kartik\rating\StarRating;
use \app\modules\events\models\Vote;


$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBCepoa6V5kvv9oRHtMkslf3hPILT5a45Y&callback=initMap');
$this->registerCssFile('/scripts/rateit.css');
$this->registerJs('$(document).ready(function () {
    var i=0;
    var prev;
    $(document).scroll(function () {

        if  ($(window).scrollTop() > $(document).height() - $(window).height() - 200)
        {
            if(i != null) {
                i++;
                $.ajax({
                    url:"/events/comment/lazy/'.$model[0]['id'].'?page=" + i,
                    type: "POST",

                    success: function (response) {
                       // alert(response);
                        if (prev != response) {
                            $(\'#out\').append(response);
                        } else {
                            i = null;
                        }

                        prev = response;
                    },
                    error: function () {
                    },
                    complete: function () {
                    }
                });
            }
        }
    });


});');
$this->registerJs('$(document).ready(function () {
$(\'.rateit\').rateit(\'value\','.$model[0]['rating'].');

$(\'.rating\').click(function() { 
var val = $(\'#w0\').val();
                $.ajax({
                    url:"/events/vote/"+val+"/"+'.$model[0]['id'].',
                    type: "GET",

                    success: function (response) {
                    
                    },
                    error: function () {
                    
                    },
                    complete: function () {
                    }
                });

});
});');

?>
<style>
    .prokrutka {
        height: 200px;
        width: 100%
        background: #fff;
        border: 1px solid #C1C1C1;

        overflow-y: scroll;
    }
</style>
<script type="text/javascript">


    var map;


    function initMap() {

        var mapOptions = {
            center: {lat: <?= $model[0]['lat']?>, lng: <?= $model[0]['lng']?>},
            zoom: 7,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false
        };


        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var marker = new google.maps.Marker({
            position: {lat: <?= $model[0]['lat']?>, lng: <?= $model[0]['lng']?>},
            map: map,
            title: '<?= $model[0]['name']?>'
        });

    }

</script>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="media-body">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <h1>  <?php
                        $event = htmlentities($model[0]['name']);
                        echo nl2br($event);
                        ?> </h1>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-6">
                    <div align="right" style="padding-top: 10px;">
                        <?php if(!Yii::$app->user->isGuest): ?>
                            <?php if(UsersEvents::canGo(Yii::$app->user->identity->getId(), $model[0]['id'])): ?>
                                    <a href="<?= Url::to(["/events/accept/".$model[0]['id']]) ?>" type="button"
                                       class="btn btn-primary"><?=Yii::t('common','I will go')?></a>
                            <?php endif; ?>
                            <?php if(!UsersEvents::canGo(Yii::$app->user->identity->getId(), $model[0]['id'])): ?>

                                <a href="<?= Url::to(["/events/dismiss/".$model[0]['id']]) ?>" type="button"
                                   class="btn btn-primary"><?=Yii::t('common','Cant go')?></a>

                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if(!Yii::$app->user->isGuest): ?>
                            <?php if(Yii::$app->user->identity->getId() == $model[0]['author_id']): ?>

                                <a href="<?= Url::to(["/events/edit/".$model[0]['id']]) ?>" type="button"
                                   class="btn btn-primary"><?=Yii::t('common','Edit')?></a>



                                <a data-toggle="modal"
                                   data-target="#modal" type="button"
                                   class="btn btn-primary" id="del"><?=Yii::t('common','Delete')?>
                                    
                                    <input type="hidden" id="<?= $model[0]['id'] ?>"
                                           value="<?= $model[0]['id'] ?>">
                                </a>



                            <?php endif; ?>
                        <?php endif; ?>

                    </div>
                    <div align="right">
                        </br>


<?php

echo StarRating::widget([
    'name' => 'rating_1',
    'value' => $model[0]['rating'],

    'pluginOptions' => [
        'step' => 1,

        'disabled'=>(Vote::canVote((!Yii::$app->user->isGuest)?Yii::$app->user->identity->getId():null, $model[0]['id']))?false:true,
        'showClear'=>false,
        'showCaption' => false,
    ]
]);
?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-body">
        <div class="media-body">
            <div class="row">
                <div class="col-lg-5 col-xs-12">
                    <?php if($model[0]['img'] != null): ?>
                        <img class="media-object img-responsive" src="
                                         <?= $model[0]->getFull() ?>"  alt="ava">
                    <?php endif; ?>
                    <?php if($model[0]['img'] == null): ?>
                        <img class="img-responsive" src="http://10.10.54.150/images/img.jpg" style="max-width: 400px"
                             alt="ava">
                    <?php endif; ?>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-6">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="media-body" style="word-break: break-all;">
                                    <h4 >
                                        <?php
                                        $event = htmlentities($model[0]['content']);
                                        echo nl2br($event);
                                        ?>

                                    </h4 >
                                </div>
                            </div>
                        </div>


                    <div class="col-lg-12 col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row" style="padding-top: 10px">

                                    <div class="col-sm-12 col-md-4 col-lg-6" style="padding-left: 40px">
                                        <h5><?=Yii::t('common','Action:')?> </br><?= $model[0]['date_action'] ?></h5>
                                    </div>

                                    <div class="col-sm-12 col-md-4 col-lg-6" align="right" style="padding-right: 40px">
                                        <h5><?=Yii::t('common','Registration end:')?> </br><?= $model[0]['date_registr']; ?></h5>
                                    </div>
                                    <img src="<?= Url::to(['/site/qrcode'])?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="media-body">
            <div class="row">
                <div class="col-lg-7 col-xs-12">
                    <div align="center" id="map" style="height: 300px; width:100%; padding-bottom: 300px;"></div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <div align="center">

                        <h3>
                            <?php if(count($model[0]['users'])>0){
                                echo count($model[0]).' '.Yii::t('common','people will go:');
                            }else{
                                echo Yii::t('common','No one go yet');
                            } ?>
                        </h3>
                    </div>
                    <div class="prokrutka" align="center">
                        <?php foreach($model[0]['users'] as $value): ?>
                            <a href="<?= Url::to(["/users/profile/".$value['id']]) ?>">
                                <?= $value['firstname'] ?> <?= $value['lastname'] ?></a> </br>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-12">



    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 align="center"><?=Yii::t('common','Comments')?> </h3>
        </div>

        <div class="panel-body" id="out">
            <?php if(!Yii::$app->user->isGuest): ?>
            <?php $form = ActiveForm::begin([
                'action' => '/events/comment/comment',
                'id' => 'data-form',
                'method' => 'post',
                'enableAjaxValidation' => true,
                'options' => ['class' => 'form-horizontal',],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-8 col-md-offset-2\">{input}</div>\n<div class=\"col-lg-4 col-lg-offset-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-3 control-label'],
                ],
            ]);
            ?>

            <?= $form->field($comments, 'content')->textarea(['rows' => 10, 'style' => ['resize' => 'none']])->label(false) ?>
            <div hidden>
                <?= $form->field($comments, 'event_id')->textInput(['readonly' => true, 'value' => $model[0]['id']])->label(false) ?>
            </div>


            <div class=" form-group">
                <div class="col-lg-offset-5 col-lg-12">
                    <?= Html::submitButton(Yii::t('common','Send'), ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'event_b']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <?php endif; ?>


            </div>

            
</div>

        <?php
$this->registerJsFile('/scripts/jquery.rateit.min.js');

?>