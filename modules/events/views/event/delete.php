<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 15.09.16
 * Time: 16:49
 */
use yii\helpers\Url;

?>

<div align="center"> <?= Yii::t('common', 'Do you really want delete this event?') ?></div>
</br>
<div class="row" align="center">
    <a href="<?= Url::to(["/events/delete-event/".$id]) ?>" type="button"
       class="btn btn-primary"><?= Yii::t('common', 'Yes') ?></a>

    <a href="<?= Url::to(["/events/show/".$id]) ?>" type="button"
       class="btn btn-primary"><?= Yii::t('common', 'No') ?></a>
</div>