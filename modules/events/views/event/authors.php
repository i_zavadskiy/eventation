<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 29.07.16
 * Time: 17:48
 */
use yii\helpers\Html;
use yii\helpers\Url;

function cutStr($str, $length = 50, $postfix = '...')
{
    if(strlen($str) <= $length)
        return $str;

    $temp = substr($str, 0, $length);
    if(strrpos($temp, ' ')) {
        return substr($temp, 0, strrpos($temp, ' ')).$postfix;
    } else {
        return $temp.$postfix;
    }
}

?>
<div class="col-lg-8 col-lg-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading"><h3 align="center"><?= Yii::t('common', 'Your events') ?></h3></div>
        <div class="panel-body">
            <?php if($model != null): ?>
                <?php for($i = (count($model) - 1); $i >= 0; $i--): ?>

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="media">
                                <a class="pull-left" href="<?= Url::to(["/events/show/".$model[$i]->id]) ?>">
                                    <?php if($model[$i]->img != null): ?>
                                        <img class="media-object" src="<?= $model[$i]->getThumb() ?>"
                                             style="max-width: 100px"
                                             alt="ava">
                                    <?php endif; ?>
                                    <?php if($model[$i]->img == null): ?>
                                        <img class="media-object" src="http://10.10.54.150/images/img.jpg"
                                             style="max-width: 100px" alt="ava">
                                    <?php endif; ?>

                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><a
                                            href="<?= Url::to(["/events/show/".$model[$i]->id]) ?>"><?= $model[$i]->name ?></a>
                                    </h4>
                                    <div class="test">
                                        <?php
                                        $event = htmlentities($model[$i]->content);
                                        echo cutStr($event)
                                        ?>

                                        <p class="text-right"><?php
                                            for($j = 0; $j < 10; $j++) {
                                                echo $model[$i]->created_at[$j];
                                            }
                                            ?>
                                        </p>

                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                <?php endfor; ?>
            <?php endif; ?>
            <?php if($model == null): ?>
                <div align="center">
                    <?= Yii::t('common', 'You havent any events =(') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>