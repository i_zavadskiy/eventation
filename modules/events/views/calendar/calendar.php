<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.08.16
 * Time: 15:15
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveField;

$this->registerJsFile('/js/calendar.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs('$(document).ready(function () {

$(\'#calendar-month\').change(function(){
        $(\'form\').submit();
});

$(\'#next\').click(function(){
if('.$month.'!=12){
$(\'#calendar-month\').val('.$month.'+1);
}else{
if('.$year.'!=2018){
$(\'#calendar-month\').val(1);
$(\'#calendar-year\').val('.$year.'+1);
}else{
$(\'#next\').popover(\'toggle\')
}
}
  $(\'form\').submit();
});

$(\'#prev\').click(function(){
if('.$month.'!=1){
$(\'#calendar-month\').val('.$month.'-1);
}else{
if('.$year.'!=2016){
$(\'#calendar-month\').val(12);
$(\'#calendar-year\').val('.$year.'-1);
}else{
$(\'#prev\').popover(\'toggle\')
}
}
  $(\'form\').submit();
  
});
});

');
$this->registerJs(' 
$(\'.ev\').click(function(){

    $.ajax({
        url: "/events/calendar/day/"+$(this).children(\'h1\').html()+"/'.$month.'/'.$year.'",
        type: "GET",
        success: function (response) {
            $(\'#h_reg\').html(\'Day\')
            $(\'.modal-body\').html(response);

        },
        error: function () {

        },
        complete: function () {

        }
    });

});

');

$array = array_chunk($events, 7, true);

?>
<style>
    .scrol {
        height: 90px;
        width: 120px;
        background: #baec7e;
        border: 1px solid #C1C1C1;

    }

    .hover1 {
        height: 80px;
        width: 110px;
        background: #baec7e;
        border: 1px solid #C1C1C1;

    }

    .hover2 {
        height: 80px;
        width: 110px;
        background: #c4e3f3;
        border: 1px solid #C1C1C1;
    }

    .scrol2 {
        height: 90px;
        width: 120px;
        background: #c4e3f3;
        border: 1px solid #C1C1C1;
    }

    .scrol3 {
        height: 90px;
        width: 120px;
        background: #e7e7e7;
        border: 1px solid #C1C1C1;
    }
</style>

<div class="row col-lg-offset-2" style="visibility: hidden; display: none;">
    <div class="col-lg-4">
        <?php $form = ActiveForm::begin([
            'action'=>'/events/calendar/show',
            'method' => 'get',
            'id' => 'calendar-form',
            'enableAjaxValidation' => false,
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-lg-4 col-lg-offset-4\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
        $model->month = $month;
        $model->year = $year;
        ?>


        <?=
        $form->field($model, 'month')
            ->dropDownList([
                '1' => 'January',
                '2' => 'February',
                '3' => 'March',
                '4' => 'April',
                '5' => 'May',
                '6' => 'June',
                '7' => 'July',
                '8' => 'August',
                '9' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',

            ]);
        ?>
    </div>
    <div class="col-lg-4">
        <?=
        $form->field($model, 'year')
            ->dropDownList([
                '2016' => '2016',
                '2017' => '2017',
                '2018' => '2018',


            ]);
        ?>
    </div>

    <div class=" form-group">
        <div class=" col-lg-1 hidden">
            <?= Html::submitButton('Show', ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'calendar_b']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>


<div class="row">
    <div class="col-lg-4" align="right">
        <h1><a id="prev" style="cursor: pointer" data-container="body" data-toggle="popover" data-placement="left"
               data-content="there is no more date"> < </a></h1>
    </div>
    <div class="col-lg-4" align="center">
        <h1>
          <?=  Yii::t('common',$name);?> <?= $year ?>
        </h1>
    </div>
    <div class="col-lg-4">
        <h1><a id="next" style="cursor: pointer" data-container="body" data-toggle="popover" data-placement="right"
               data-content="there is no more date"> > </a></h1>

    </div>
</div>

<table align="center" cellspacing="0" border="0" cellpadding="0">
    <tr>
        <td align="center"><h2><?=  Yii::t('common','Mon')?></h2></td>
        <td align="center"><h2><?=  Yii::t('common','Tues')?></h2></td>
        <td align="center"><h2><?= Yii::t('common','Wed')?></h2></td>
        <td align="center"><h2><?=  Yii::t('common','Thurs')?></h2></td>
        <td align="center"><h2><?=  Yii::t('common','Fri')?></h2></td>
        <td align="center"><h2><font color="#8b0000"><?=  Yii::t('common','Sat')?></h2></font></td>
        <td align="center"><h2><font color="#8b0000"><?=  Yii::t('common','Sun')?></h2></font></td>
    </tr>

    <?php
    foreach($array as $week) {
        echo '<tr>';
        foreach($week as $key => $value) {
            $outs = '';
            $a = 0;
            echo '<td>';
            if($value == null) {

                if(preg_replace('/-[0-9]+/', '', $key) == $month) {
                    echo "<div align='center' class=\"panel-body\"><div class=\"scrol2\">";
                    echo '<h1 align="center">'.preg_replace('/^[0-9]+-/', '', $key).'</h1>'.'</br>';
                } else {
                    echo "<div class=\"panel-body\"><div class=\"scrol3\">";
                    echo '<h1 align="center"><font color="#778899">'.preg_replace('/^[0-9]+-/', '', $key).'</font></h1>'.'</br>';
                }
            }
            if($value != null) {
                for($j = 0; $j < count($value); $j++) {
                    $outs .= $value[$j]['name'].'</br>';
                    $a++;
                }
                echo "<div align='center' class=\"panel-body \" data-toggle=\"modal\"
                                                          data-target=\"#modal\"><div class=\"ev scrol\">";

                $e = ($a>1)?' events':' event';
                echo '<h1 align="center">'.preg_replace('/^[0-9]+-/', '', $key).'</h1> <span class="badge">'.$a.$e.'</span>'.'</br>';


            }
            echo "</div>";

            echo '</td>';
        }
        echo '</tr>';

    }


    ?>


</table>
