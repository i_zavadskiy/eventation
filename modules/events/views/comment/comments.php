<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 08.08.16
 * Time: 11:36
 */
use app\modules\users\models\User;
use Yii;
use yii\helpers\Url;
if($model==null){
    echo Yii::t('common','There is no comments yet');
}
?>

<?php foreach($model as $value1): ?>
    <?php $user = User::find()->where(['id' => $value1["user_id"]])->one();

    ?>

    <div class="panel panel-default" id="main">
        <div class="panel-body">

            <div class="media">
                <a class="pull-left" href="<?= Url::to(["/users/profile/".$user->id]) ?>">
                    <?php if($user->img != null): ?>
                        <img class="media-object" src="<?= $user->getThumb() ?>" style="max-width: 100px"
                             alt="ava">
                    <?php endif; ?>
                    <?php if($user->img == null): ?>
                        <img class="media-object" src="http://10.10.54.150/images/2.png"
                             style="max-width: 100px" alt="ava">
                    <?php endif; ?>

                </a>
                <div class="media-body">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <h4 class="media-heading"><a
                                    href="<?= Url::to(["/users/profile/".$user->id]) ?>"><?= $user->firstname ?> <?= $user->lastname ?></a>
                            </h4>
                        </div>

                        <div class="col-lg-6">
                            <?php if(!Yii::$app->user->isGuest): ?>
                                <?php if($value1['user_id'] == Yii::$app->user->identity->getId()): ?>

                                    <div align="right">
                                        <span id="edit" data-toggle="tooltip" data-placement="top" title="<?=Yii::t('common', 'Edit');?>">
                                        <input type="hidden" id="<?= $value1['id'] ?>"
                                               value="<?= $value1['id'] ?>">

                                        <span class="glyphicon glyphicon-edit" data-toggle="modal"
                                              data-target="#modal" > </span>
                                        </span>
                                        <span id="del" data-toggle="tooltip" data-placement="top" title="<?=Yii::t('common', 'Delete');?>">
                                        <input type="hidden" id="<?= $value1['id'] ?>"
                                               value="<?= $value1['id'] ?>">

                                        <span class="glyphicon glyphicon-trash" data-toggle="modal"
                                              data-target="#modal" > </span>
                                        </span>
                                    </div>

                                <?php endif; ?>

                                <div align="right">
                                        <span id="claim" data-toggle="tooltip" data-placement="top" title="<?=Yii::t('common', 'Claim');?>">
                                        <input type="hidden" id="<?= $value1['id'] ?>"
                                               value="<?= $value1['id'] ?>">

                                        <span class="glyphicon glyphicon-warning-sign" data-toggle="modal"
                                              data-target="#modal"> </span>
                                        </span>
                                         <span id="answer" data-toggle="tooltip" data-placement="top" title="<?=Yii::t('common', 'Answer');?>">
                                        <input type="hidden" id="<?= $value1['id'] ?>"
                                               value="<?= $value1['id'] ?>">

                                        <span class="glyphicon glyphicon-share" data-toggle="modal"
                                              data-target="#modal"> </span>
                                        </span>
                                </div>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="test">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <?php
                                $comment = htmlentities($value1['content']);
                                echo $comment;
                                ?>

                            </div>


                        </div>
                        <p class="text-right">
                            <?php
                            echo $value1['created_at']

                            ?>


                        </p>

                    </div>

                </div>


            </div>

        </div>

    </div>

    <?php if($value1['rel']): ?>
        <?php foreach($value1['rel'] as $value): ?>
            <?php $user = User::find()->where(['id' => $value["user_id"]])->one(); ?>

            <div class="panel panel-default col-lg-11 col-lg-offset-1">
                <div class="panel-body">

                    <div class="media2">
                        <a class="pull-left" href="<?= Url::to(["/users/profile/".$user->id]) ?>">
                            <?php if($user->img != null): ?>
                                <img class="media-object" src="<?= $user->getThumb() ?>" style="max-width: 100px"
                                     alt="ava">
                            <?php endif; ?>
                            <?php if($user->img == null): ?>
                                <img class="media-object" src="http://10.10.54.150/images/2.png"
                                     style="max-width: 100px" alt="ava">
                            <?php endif; ?>

                        </a>

                        <div class="media-body">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <h4 class="media-heading"><a
                                            href="<?= Url::to(["/users/profile/".$user->id]) ?>"><?= $user->firstname ?> <?= $user->lastname ?></a>
                                    </h4>
                                </div>

                                <div class="col-lg-6">
                                    <?php if(!Yii::$app->user->isGuest): ?>
                                        <?php if($value['user_id'] == Yii::$app->user->identity->getId()): ?>

                                            <div align="right">
                                        <span id="edit">
                                        <input type="hidden" id="<?= $value['id'] ?>"
                                               value="<?= $value['id'] ?>">

                                        <span class="glyphicon glyphicon-edit" data-toggle="modal"
                                              data-target="#modal"> </span>
                                        </span>
                                                <a style="color: black"
                                                   href="<?= Url::to(["/events/comment/delete-comment/".$value['id']]) ?>">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </div>

                                        <?php endif; ?>

                                        <div align="right">
                                        <span id="claim">
                                        <input type="hidden" id="<?= $value['id'] ?>"
                                               value="<?= $value['id'] ?>">

                                        <span class="glyphicon glyphicon-warning-sign" data-toggle="modal"
                                              data-target="#modal"> </span>
                                        </span>

                                        </div>
                                    <?php endif; ?>
                                </div>

                            </div>

                            <div class="test">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <?php
                                        $comment = htmlentities($value['content']);
                                        echo $comment;
                                        ?>
                                    </div>


                                </div>
                                <p class="text-right">
                                    <?php
                                    echo $value['created_at']

                                    ?>


                                </p>

                            </div>

                        </div>


                    </div>

                </div>

            </div>
        <?php endforeach; ?>
<div>&nbsp;</div>
    <?php endif; ?>
<?php endforeach; ?>
