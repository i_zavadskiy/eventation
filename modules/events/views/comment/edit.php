<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 10.08.16
 * Time: 12:43
 */

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'action' => '/events/comment/edit-comment',
    'id' => 'data-form',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal',],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-8 col-md-offset-2\">{input}</div>\n<div class=\"col-lg-4 col-lg-offset-4\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]);
?>

<?= $form->field($comment, 'content')->textarea(['rows' => 10, 'style' => ['resize' => 'none']])->label(false) ?>
<div hidden>
    <?= $form->field($comment, 'id')->textInput(['readonly' => true, 'value' => $comment->id]) ?>
    <?= $form->field($comment, 'created_at')->textInput(['readonly' => true, 'value' => $comment->created_at]) ?>
</div>


<div class=" form-group">
    <div class="col-lg-offset-5 col-lg-11">
        <?= Html::submitButton(Yii::t('common', 'Send'), ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'event_b']) ?>
    </div>

    <?php ActiveForm::end(); ?>
