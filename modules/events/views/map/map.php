<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 18.10.16
 * Time: 15:46
 */

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBCepoa6V5kvv9oRHtMkslf3hPILT5a45Y&callback=initMap', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div align="center" id="map" style="height: 800px; width:100%; padding-bottom: 300px;"></div>

<script>

    var map;


    function initMap() {

        var mapOptions = {
            center: {lat: 48.379433, lng: 31.16558},
            zoom: 6,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false
        };

        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        $.ajax({
            url: "/events/map/marker",
            type: "GET",
            success: function (response) {
                var m = JSON.parse(response);
                console.log(m);
m.forEach(function(item){
console.log(item);
                new google.maps.Marker({
                    position: {lat: item['lat'], lng: item['lng']},
                    map: map,
                    title: item['name']
                });
});

            },
            error: function () {
            },
            complete: function () {
            }
        });
//            new google.maps.Marker({
//                position: {lat: 42, lng: 42},
//                map: map,
//                title: 'df'
//            });

        }

</script>


<script>








</script>
