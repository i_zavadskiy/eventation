<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 11.08.16
 * Time: 15:13
 */

namespace app\modules\events\controllers;


use app\modules\events\models\Calendar;
use app\modules\events\models\Events;
use yii\web\Controller;
use yii;

class CalendarController extends Controller
{

    public function actionShow()
    {

        $calendar = new Calendar();

        $arrDate = Yii::$app->request->get('Calendar');
        if(isset($arrDate)) {
            if (array_key_exists('year', $arrDate) && array_key_exists('month', $arrDate)) {
                if ($calendar->dateValidate($arrDate['year'], $arrDate['month'])) {
                    $month = Yii::$app->request->get('Calendar')['month'] ? Yii::$app->request->get('Calendar')['month'] : date('n');
                    $year = Yii::$app->request->get('Calendar')['year'] ? Yii::$app->request->get('Calendar')['year'] : date('Y');
                } else {
                    $month = date('n');
                    $year = date('Y');
                }
            } else {
                $month = date('n');
                $year = date('Y');
            }
        }else {
            $month = date('n');
            $year = date('Y');
        }


            $monthName = (new \DateTime($year . '-' . $month))->format('F');
            $firstDay = (new \DateTime($year . '-' . $month))->modify('first day of this month')->format('j');
            $lastDay = (new \DateTime($year . '-' . $month))->modify('last day of this month')->format('j');

            if ((new \DateTime($year . '-' . $month))->modify('first day of this month')->format('D') != 'Mon') {
                $startCalendar = (new \DateTime($year . '-' . $month))->modify('last mon')->format("Y-n-j");
            } else {
                $startCalendar = (new \DateTime($year . '-' . $month))->modify('first day of this month')->format("Y-n-j");
            }

            $events = Events::find()->where('date_action >= :date1', ['date1' => (new \DateTime($year . '-' . $month . '-' . $firstDay))->format('Y-m-d H:i:s')])->
            andWhere('date_action <= :date2', ['date2' => (new \DateTime($year . '-' . $month . '-' . $lastDay))->format('Y-m-d H:i:s')])->
            all();


            return $this->render('calendar',
                ['events' => $calendar->createMonth($events, $startCalendar),
                    'month' => $month,
                    'year' => $year,
                    'model' => $calendar,
                    'name' => $monthName,
                ]);

    }


    public function actionDay()
    {
        $calendar = new Calendar();

        $events = Events::find()->where('date_action >= :date1',
            ['date1' => (new \DateTime(Yii::$app->request->get('y').'-'.Yii::$app->request->get('m').'-'.Yii::$app->request->get('d').' 00:00:00'))->format('Y-m-d H:i:s')])->
        andWhere('date_action <= :date2', ['date2' => (new \DateTime(Yii::$app->request->get('y').'-'.Yii::$app->request->get('m').'-'.Yii::$app->request->get('d').' 23:59:59'))->format('Y-m-d H:i:s')])->
        orderBy('date_action')->all();
        return $this->renderAjax('day',['events'=>$calendar->createDay($events)]);
    }
}