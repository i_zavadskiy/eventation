<?php

/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 28.07.16
 * Time: 11:06
 */

namespace app\modules\events\controllers;

use app\models\ImageUpload;
use app\modules\events\models\Comments;
use app\modules\events\models\Event;
use app\modules\events\models\ImageCropEvent;
use app\modules\events\models\UsersEvents;
use app\modules\events\models\Vote;
use yii;
use app\modules\events\models\Events;
use yii\web\Controller;
use app\scripts\AjaxValidator;
use app\modules\users\models\User;
use yii\helpers\Url;
use yii\filters\AccessControl;


class EventController extends Controller
{
    use AjaxValidator;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['show'],
                        'roles'=>['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],

        ];
    }


    public function actionCreateEvent()
    {
        $model = new Events();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } else {

                if($_FILES['Events']['name']['imageFile']!='') {
                    if (mime_content_type($_FILES['Events']['tmp_name']['imageFile']) == "image/jpeg"
                        || mime_content_type($_FILES['Events']['tmp_name']['imageFile']) == "image/png"
                    ) {

                        if (Yii::$app->request->post('Events')['weight'] != null) {
                            $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                                'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);
                        } else {
                            $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                                'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);
                        }
                        $crop = new ImageCropEvent($imageModel->file_name, UPLOAD_PATH . '/' . $imageModel->get_upload_path());

                        $crop->crop(Yii::$app->request->post('Events')['coordX'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['coordY'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['height'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['weight'] * 100 / Yii::$app->request->post('Events')['per'],
                            100, 'thumb'
                        );
                        $crop->crop(Yii::$app->request->post('Events')['coordX'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['coordY'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['height'] * 100 / Yii::$app->request->post('Events')['per'],
                            Yii::$app->request->post('Events')['weight'] * 100 / Yii::$app->request->post('Events')['per'],
                            800, 'medium'
                        );
                        $model->img = $imageModel->file_name;
                    }

                }
                $model->count = 1;
                $model->setID();
                $model->save();

                Yii::$app->session->setFlash('success',  Yii::t('common', 'Your event will be great!'));
                return $this->redirect('/');
            }

        }

        return $this->render('create', ['model' => $model]);
    }


    public function actionMyConfirmed()
    {
        $model2 = new UsersEvents();
        $events = $model2->whereGo(Yii::$app->user->identity->getId());

        return $this->render('confirm', ['model' => $events]);
    }


    public function actionMyEvents()
    {
        $model = new Events();

        return $this->render('authors', ['model' => $model->getAuthEvents(Yii::$app->user->identity->getId())]);
    }


    public function actionShow()
    {
        $model = Events::find()->with(['users'])->where(['id' => Yii::$app->request->get('id')])->limit(10)->all();
        $comment = new Comments();
        if($model!=null) {
            return $this->render('oneEvent', ['model' => $model, 'comments' => $comment]);
        }else{
            return $this->redirect('/');
        }

    }


   


    public function actionEdit()
    {

            $model = Events::find()->where(["id" => Yii::$app->request->get('id')])->one();
        if($model!=null) {
        if(Yii::$app->user->identity->getId() == $model->author_id) {
            if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
                if(Yii::$app->request->isAjax) {
                    $this->validateQuery($model);
                } else {

                    if($_FILES['Events']['name']['imageFile'] != '') {
                        if($_FILES['Events']['name']['imageFile']!='') {

                        }
                            if (mime_content_type($_FILES['Events']['tmp_name']['imageFile']) == "image/jpeg"
                                || mime_content_type($_FILES['Events']['tmp_name']['imageFile']) == "image/png"
                            ) {
                                if (Yii::$app->request->post('Events')['weight'] != null) {
                                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                                        'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);
                                } else {
                                    $imageModel = new ImageUpload(['upload_dir' => 'uploads/event/',
                                        'upload_url' => 'uploads/event/', 'param_name' => 'Events',]);
                                }

                                $crop = new ImageCropEvent($imageModel->file_name, UPLOAD_PATH . '/' . $imageModel->get_upload_path());

                                $crop->crop(Yii::$app->request->post('Events')['coordX'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['coordY'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['height'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['weight'] * 100 / Yii::$app->request->post('Events')['per'],
                                    100, 'thumb'
                                );
                                $crop->crop(Yii::$app->request->post('Events')['coordX'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['coordY'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['height'] * 100 / Yii::$app->request->post('Events')['per'],
                                    Yii::$app->request->post('Events')['weight'] * 100 / Yii::$app->request->post('Events')['per'],
                                    800, 'medium'
                                );

                                $model->img = $imageModel->file_name;
                            }
                    }
                    $model->update();
                    Yii::$app->session->setFlash('success',  Yii::t('common', 'Your event will be great!'));
                    return $this->redirect('/');

                }
            }

            return $this->render('create', ['model' => $model]);

        }
        }else{
            return $this->redirect('/');
        }
    }

    public function actionPreDelete(){
        return $this->renderAjax('delete',['id'=>Yii::$app->request->get('id')]);
    }

    public function actionDeleteEvent()
    {

            $event = Events::findOne(Yii::$app->request->get('id'));
        if($event!=null) {

            $event->delete();
            Yii::$app->session->setFlash('success', Yii::t('common', 'Your event was deleted'));
        }
            return $this->redirect('/');

    }


    public function actionAccept()
    {
        $user = Yii::$app->user->identity->getId();
        $event = Events::find()->where(["id" => Yii::$app->request->get('id')])->one();
        $modelSave = new UsersEvents();

        if($event!=null) {

            $currentDate = date("Ymd");
            $registerDate = date("Ymd", strtotime($event->date_registr));
            if (UsersEvents::canGo($user, $event->id)) {
                if ($registerDate > $currentDate) {
                    $event->count += 1;
                    $modelSave->user_id = $user;
                    $modelSave->event_id = $event->id;
                    $modelSave->save();
                    $event->update();
                    Yii::$app->session->setFlash('success', Yii::t('common', 'Have a good time!'));

                } else {

                    Yii::$app->session->setFlash('error', Yii::t('common', 'Time over'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('common', 'You already go'));
            }
        }

        return $this->redirect(Url::to(["/events/show/".$event->id]));
    }


    public function actionDismiss()
    {
        $event = Events::find()->where(["id" => Yii::$app->request->get('id')])->one();
        if($event!=null) {
            $model = UsersEvents::find()->where(['user_id' => Yii::$app->user->identity->getId(), 'event_id' => Yii::$app->request->get('id')])->one();
            $event->count -= 1;
            $event->save(false);
            $model->delete();
        }



        return $this->redirect(Url::to(["/events/show/".Yii::$app->request->get('id')]));
    }

    public function actionVote(){

        if(Vote::find()->where(['user_id'=>Yii::$app->user->identity->getId()])->andWhere(['event_id'=>Yii::$app->request->get('id')])->one() != null){
            Yii::$app->session->setFlash('error',  Yii::t('common', 'You cant vote!'));
        }else{
            $model = new Vote();
            $model->user_id = Yii::$app->user->identity->getId();
            $model->event_id = Yii::$app->request->get('id');
            $model->mark = Yii::$app->request->get('r');
            $model->save();
            $model->count(Yii::$app->request->get('id'));
            Yii::$app->session->setFlash('success',  Yii::t('common', 'Thank you for vote!'));

        }

        return $this->redirect(Url::to(["/events/show/".Yii::$app->request->get('id')]));


    }
}