<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 15.08.16
 * Time: 15:00
 */

namespace app\modules\events\controllers;


use app\modules\events\models\Claims;
use app\modules\events\models\Comments;
use yii;
use yii\helpers\Url;
use app\scripts\AjaxValidator;
use yii\data\Pagination;
use yii\filters\AccessControl;

class CommentController extends yii\web\Controller
{
    use AjaxValidator;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['lazy'],
                        'roles'=>['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],

        ];
    }

    public function actionComment()
    {
        $comment = new Comments();

        if(Yii::$app->request->isPost && $comment->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($comment);
            } else {
                $comment->setID();
                $comment->setPostId(Yii::$app->request->post("Comments")["event_id"]);
                $comment->save();
                return $this->redirect(Url::to(["/events/show/".Yii::$app->request->post("Comments")["event_id"]]));
            }
        }
    }

    public function actionPreDelete(){
        $comment = Comments::findOne(Yii::$app->request->get('id'));
        return $this->renderAjax('delete',['comment'=>$comment]);
    }


    public function actionDeleteComment()
    {

        $comment = Comments::findOne(Yii::$app->request->get('id'));
        if(Yii::$app->user->identity->getId() == $comment->user_id) {
            $event_id = $comment->event_id;
            $comment->delete();
            return $this->redirect(Url::to(["/events/show/".$event_id]));
        }
    }


    public function actionEditComment()
    {

        if(Yii::$app->request->isGet) {

            $comment = Comments::findOne(Yii::$app->request->get('id'));
            if(!Yii::$app->user->identity->getId() == $comment->user_id) {
                return $this->redirect('/');
            }
        } else {

            $comment = Comments::findOne(Yii::$app->request->post('Comments')['id']);
            if(!Yii::$app->user->identity->getId() == $comment->user_id) {
                return $this->redirect('/');
            }
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($comment);
            }

            if(Yii::$app->request->post()) {
                $comment->content = Yii::$app->request->post('Comments')['content'];
                $comment->created_at = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post('Comments')['created_at']));
                $comment->update();
                $event_id = $comment->event_id;

                return $this->redirect(Url::to(["/events/show/".$event_id]));
            }
        }

        return $this->renderAjax('edit', ['comment' => $comment]);
    }


    public function actionClaimComment()
    {

        $model = new Claims();
        $id = Yii::$app->request->get('id');
//        var_dump($_REQUEST);
//        exit;
        if(Yii::$app->request->post()) {

            $model->comment_id  = Yii::$app->request->post('Claims')['comment_id'];
            $model->type  = Yii::$app->request->post('Claims')['type'];
            $model->content  = Yii::$app->request->post('Claims')['content'];
            $model->save();
            $id = Comments::findOne($model->comment_id)->event_id;
            Yii::$app->session->setFlash('success', Yii::t('common', 'Thank you! We will check this comment soon'));
            return $this->redirect(Url::to(["/events/show/".$id]));
        }
        return $this->renderAjax('claim', ['model' => $model,
            'id' => $id]);
    }

    public function actionAnswerComment(){

        $model = new Comments();


        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())){
            var_dump('lol');
            $model->setID();
            $model->setRelative(Yii::$app->request->post("Comments")['relative']);
            $comment = Comments::findOne(Yii::$app->request->post("Comments")['relative']);
            $model->setPostId($comment['event_id']);
            $model->save();
            return $this->redirect(Url::to(["/events/show/".$model->event_id]));
        }
        return $this->renderAjax('answer',['model'=>$model,'id'=>Yii::$app->request->get('id')]);
    }


    public function actionLazy()
    {

        $showComments = Comments::find()->joinWith('rel')->where(['comments.relative'=>null])
            ->andWhere(['comments.event_id'=>Yii::$app->request->get('id')])->orderBy(['comments.created_at' => SORT_DESC]);



        $countQuery = clone $showComments;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 1,
        ]);
        $models = $showComments->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->layout = false;
        return $this->render('comments', [
            'model' => $models, 'pages' => $pages,
        ]);

    }
}