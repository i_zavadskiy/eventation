<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 18.10.16
 * Time: 15:36
 */

namespace app\modules\events\controllers;


use app\modules\events\models\Calendar;
use app\modules\events\models\Events;
use yii\web\Controller;
use yii;

class MapController extends Controller
{

    public function actionShow(){



        return $this->render('map');
    }

    public function actionMarker(){
        $month = date('n');
        $year = date('Y');
        $firstDay = (new \DateTime($year . '-' . $month))->modify('first day of this month')->format('j');
        $events = Events::find()->where('date_action >= :date1', ['date1' => (new \DateTime($year . '-' . $month . '-' . $firstDay))->format('Y-m-d H:i:s')])->all();
        $array = [];
        foreach ($events as $value){
          $array[] = [
              'lat' => $value['lat'],
              'lng' => $value['lng'],
              'name' => $value['name'],
          ];
        }

        echo \GuzzleHttp\json_encode($array);

    }
}