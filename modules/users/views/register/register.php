<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use dosamigos\datetimepicker\DateTimePicker;

//$this->title = 'Register';
//$this->params['breadcrumbs'][] = $this->title;
?>


<?php $form = ActiveForm::begin([
    'id' => 'register-form',
    'enableAjaxValidation' => true,
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]); ?>

<?= $form->field($model, 'email')->textInput()->hint('Enter, your email')->label(Yii::t('common', 'Email*')) ?>

<?= $form->field($model, 'password')->passwordInput()->hint('Enter, your password')->label(Yii::t('common', 'Password*')) ?>
<?= $form->field($model, 'password2')->passwordInput()->hint('Enter, your password')->label(Yii::t('common', 'Password again*')) ?>

<?= $form->field($model, 'firstname')->textInput()->hint('Enter, your first name')->label(Yii::t('common', 'First name*')) ?>

<?= $form->field($model, 'lastname')->textInput()->hint('Enter, your last name')->label(Yii::t('common', 'Last name*')) ?>


<?= $form->field($model, 'dob')->widget(DateTimePicker::className(), [


    'template' => '{input}',
    'pickButtonIcon' => 'glyphicon glyphicon-time',
    'inline' => false,
    'clientOptions' => [
        'defaultViewDate' => ['year' => 2000],
        'initialDate' => '2000-01-01 00:00:00',
        'startDate' => '1950-01-01 00:00:00',
        'endDate' => '2010-01-01 00:00:00',
        'startView' => 4,
        'minView' => 2,
        'maxView' => 4,
        'autoclose' => true,
        'format' => 'yyyy-mm-dd hh:ii:ss',

    ]
])->label(Yii::t('common', 'Date action*'));?>

<div id="load"></div>
<div class="form-group">

    <div class="col-lg-offset-5 col-lg-11">
        <?= Html::submitButton(Yii::t('common', 'Sign Up'), ['class' => 'btn btn-primary loading', 'name' => 'login-button', 'id' => 'register_b']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
