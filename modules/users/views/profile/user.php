<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 04.08.16
 * Time: 15:12
 */


?>


<div class="avatar col-md-3">
    <?php if($model->img == null): ?>
        <img src="http://10.10.54.150/images/2.png" class="img-rounded" style="max-width: 200px"
             alt="Отзывчивое изображение в Bootstrap">
    <?php endif ?>
    <?php if($model->img != null): ?>
        <img src="<?= $model->getFull() ?>" class="img-rounded" style="max-width: 200px"
             alt="Отзывчивое изображение в Bootstrap">
    <?php endif ?>
</div>

<div class="col-md-7 col-md-offset-1 name">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1><?php echo $model['firstname'] ?><?php echo $model['lastname'] ?></h1></div>
        <div class="panel-body">
            <ul>
                <li><h4><?= Yii::t('common', 'Date of birth:');?> <?php echo $model['dob'] ?></h4></li>
                <?php if($model['city']!=null): ?>
                <li><h4><?= Yii::t('common', 'City:'); ?> <?php echo $model['city'] ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6 col-md-offset-2  col-md-pull name">


</div>
