<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;

$this->registerJsFile('/js/jquery.Jcrop.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/imageCrop.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile('/css/jquery.Jcrop.min.css');
?>


<div>
    <!-- Навигация -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?= Yii::t('common', 'Profile') ?> <span
                    class="glyphicon glyphicon-user"></a></li>
        <li><a href="#edit" aria-controls="edit" role="tab" data-toggle="tab"><?= Yii::t('common', 'Data') ?> <span
                    class="glyphicon glyphicon-list-alt"></a></li>
        <li><a href="#password" aria-controls="password" role="tab" data-toggle="tab"><?= Yii::t('common', 'Password')?> <span
                    class="glyphicon glyphicon-cog"></a></li>
        <li><a href="#avatar" aria-controls="avatar" role="tab" data-toggle="tab"><?= Yii::t('common', 'Avatar') ?> <span
                    class="glyphicon glyphicon-picture"></a></li>

    </ul>

    <!-- Содержимое вкладок -->
    <div class="tab-content">
        <!--        1      -->
        <div role="tabpanel" class="tab-pane active " id="profile">
            <div class="avatar col-md-3">
                <?php if($model->img == null): ?>
                    <img src="http://10.10.54.150/images/2.png" class="img-rounded" style="max-width: 200px"
                         alt="Bootstrap">
                <?php endif ?>
                <?php if($model->img != null): ?>
                    <img src="<?= $model->getMedium() ?>?id=<?=rand();?>" class="img-rounded" style="max-width: 200px"
                         alt="Bootstrap">
                <?php endif ?>
            </div>

            <div class="col-md-7 col-md-offset-1 name">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1><?php echo $model['firstname'] ?><?php echo $model['lastname'] ?></h1></div>
                    <div class="panel-body">
                        <ul>
                            <li><h4><?= Yii::t('common', 'Date of birth:');?> <?php echo $model['dob'] ?></h4></li>
                            <li><h4><?= Yii::t('common', 'City:'); ?> <?php echo $model['city'] ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-2  col-md-pull name">


            </div>

        </div>

        <!--      2                -->
        <div role="tabpanel" class="tab-pane name" id="edit">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 align="center"><?= Yii::t('common', 'Reset your personal data')?></h3></div>
                    <div class="panel-body">

                    </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'data-form',
                        'enableAjaxValidation' => true,
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                    ]); ?>



                    <?= $form->field($modelData, 'firstname')->textInput()->hint('Enter, your first name')->label(Yii::t('common','First name*')) ?>

                    <?= $form->field($modelData, 'lastname')->textInput()->hint('Enter, your last name')->label(Yii::t('common','Last name*')) ?>

                    <?= $form->field($modelData, 'city')->textInput()->hint('Enter, your city')->label(Yii::t('common','City*')) ?>

                    <?= $form->field($modelData, 'dob')->widget(DatePicker::className(),
                        array(

                            'model' => $modelData,
                            'dateFormat' => 'y-M-d',

                            'options' => array(

                                'class' => 'form-control',
                                'showAnim' => 'fadeIn',
                                'showButtonPanel' => 'true',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                            ),
                            'clientOptions' => array(
                                'autoclose' => true,
                                'showAnim' => 'fadeIn',
                                'showButtonPanel' => 'true',
                                'changeMonth' => 'true',
                                'changeYear' => 'true',
                                'yearRange' => '-70:-0',
                                'maxDate' => date('Y-m-d'),
                            )

                        ))->label(Yii::t('common','Dob*')) ?>


                    <div class="form-group">

                        <div class="col-lg-offset-5 col-lg-11">
                            <?= Html::submitButton(Yii::t('common','Save'), ['class' => 'btn btn-primary loading', 'name' => 'data-button', 'id' => 'data_b']) ?>
                        </div>


                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

            </div>

        </div>

        <!--   3              -->
        <div role="tabpanel" class="tab-pane name" id="password">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 align="center"><?= Yii::t('common', 'Reset your password')?></h3></div>
                    <div class="panel-body">

                        <?php $form = ActiveForm::begin([
                            'id' => 'password-form',
                            'enableAjaxValidation' => true,
                            'options' => ['class' => 'form-horizontal'],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-4\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-4 control-label'],
                            ],
                        ]); ?>


                        <?= $form->field($modelPassword, 'oldPassword')->passwordInput()->hint('Enter, your password')->label(Yii::t('common', 'Old password*')) ?>

                        <?= $form->field($modelPassword, 'password')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password*')) ?>

                        <?= $form->field($modelPassword, 'password2')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password again*')) ?>


                        <div class="form-group">
                            <div class="col-lg-offset-5 col-lg-11">
                                <?= Html::submitButton(Yii::t('common','Save'), ['class' => 'btn btn-primary loading', 'name' => 'password-button', 'id' => 'password_b']) ?>
                            </div>
                        </div>


                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane name" id="avatar">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 align="center"><?= Yii::t('common', 'Reset your avatar')?></h3></div>
                    <div class="panel-body ">
                        <?php $form = ActiveForm::begin([
                            'id' => 'avatar-form',
                            'enableAjaxValidation' => true,
                            'options' => [
                                'enctype' => 'multipart/form-data',
                                'class' => 'form-horizontal'],
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
                                'labelOptions' => ['class' => 'col-lg-4 control-label'],
                            ],
                        ]); ?>

                        <div class="hidden">
                        <?= $form->field($modelAvatar, 'coordX')->textInput(['type' => 'number']) ?>
                        <?= $form->field($modelAvatar, 'coordY')->textInput(['type' => 'number']) ?>
                        <?= $form->field($modelAvatar, 'height')->textInput(['type' => 'number']) ?>
                        <?= $form->field($modelAvatar, 'weight')->textInput(['type' => 'number']) ?>
                        <?= $form->field($modelAvatar, 'per')->textInput(['type' => 'number','value'=>100]) ?>
                        </div>
                        <button type="button" class="btn btn-default btn-lg" id="upload">
                            <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
                        </button>


                        <div hidden>
                        <?= $form->field($modelAvatar, 'imageFile')->fileInput()->label(Yii::t('common','Image File')); ?>
                            </div>

                        <div class="output" align="center">
                            <img src="<?= $model->getFull() ?>" class="img-rounded" style='max-width: 500px; max-height: 500px' id="target" alt="p">
                        </div>
                        <div id="error" align="center">

                        </div>
                        <script>

var jcrop_api;
$(document).ready(function(){
info()
});

function foo2(e) {

    document.getElementById('avatarform-imagefile').click();
}
document.getElementById('upload').addEventListener('click', foo2, false);


                            function info() {
                                var image = new Image();
                                image.src = $('img#target').attr('src');
                                image.onload = function(){

                                    jQuery(function ($) {
                                        jcrop_api = $.Jcrop('#target', {
                                            aspectRatio: 1,
                                            setSelect: [0,0, 100,100],
                                            minSize: [50,50],
                                            allowSelect: false,
                                            onChange: showCoords,
                                            boxWidth: 500,
                                            boxHeight: 500
                                        });
                                        function showCoords(c)
                                        {
                                            $('#avatarform-coordx').val(Math.round(c.x));
                                            $('#avatarform-coordy').val(Math.round(c.y));
                                            $('#avatarform-height').val(Math.round(c.h));
                                            $('#avatarform-weight').val(Math.round(c.w));

                                        };
                                    });

                                    if (image.width>image.height && image.width > 500) {
                                        var pers = (500 * 100) / image.width;
                                        $('#avatarform-per').val(Math.round(pers));
                                    }
                                    if (image.height>image.width && image.height > 500) {

                                        var pers = (500 * 100) / image.height;
                                        $('#avatarform-per').val(Math.round(pers));
                                    }

                                };
                                };


                            function foo(e) {
                                if( jcrop_api !== undefined) {
                                    jcrop_api.destroy();
                                }
                                var file = e.target.files[0];
                                var reader = new FileReader();
                                if (file.type == 'image/jpeg' || file.type == 'image/png') {

                                    reader.readAsDataURL(file);
                                    reader.onload = (function (file, data) {
                                        console.log(file['total']);
                                        $('#error').html("");
                                        if(file['total']>5242880){
                                            $('#error').html("<?=Yii::t('common','More then 5Mb')?>");
                                            $('#error').css('color', 'firebrick');
                                        }
                                        $('#target').attr('src', file.target.result);
                                        $('#target').attr('style', 'max-width: 500px; max-height: 500px');
                                        $('#target').attr('class', 'img-rounded');
                                        info();
                                    });
                                }
                            }

                            document.getElementById('avatarform-imagefile').addEventListener('change', foo, false);
                        </script>

                        <div class="form-group">
                            <div class="col-lg-offset-5 col-lg-11" style="margin-top: 20px;">
                                <?= Html::submitButton(Yii::t('common','Save'), ['class' => 'btn btn-primary loading', 'name' => 'avatar-button', 'id' => 'avatar_b']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end() ?>


                    </div>
                </div>
            </div>
        </div>

    </div>
