<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('common', 'Recover password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>



    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <?php if(isset($hash)): ?>
        <?= $form->field($model, 'password')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password*')) ?>
        <?= $form->field($model, 'password2')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password again*')) ?>


        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?= Html::submitButton(Yii::t('common','Save'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
    <?php endif ?>


    <?php if(!isset($hash)): ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true])->label(Yii::t('common','Email*')) ?>


        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?= Html::submitButton(Yii::t('common','Restore password'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php endif ?>

    <?php ActiveForm::end(); ?>
</div>