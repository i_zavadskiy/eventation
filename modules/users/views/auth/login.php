<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\users\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-3 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'email')->textInput()->label(Yii::t('common', 'Email*')) ?>

    <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('common', 'Password*')); ?>

    <?= $form->field($model, 'rememberMe')->checkbox([
        'template' => "<div class=\"col-lg-offset-3 col-lg-4\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
    ])->label(Yii::t('common', 'Remember me')) ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-11">
            <?= Html::submitButton(Yii::t('common', 'Sign In '), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

            <a class="col-lg-offset-4" href="<?= Url::to('/users/auth/forgot-password') ?>"><?=Yii::t('common', 'Forgot password?')?></a>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


