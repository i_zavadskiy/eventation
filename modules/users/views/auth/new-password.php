<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.09.16
 * Time: 13:10
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-6 col-lg-offset-3\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<?= $form->field($model, 'password')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password*')) ?>
<?= $form->field($model, 'password2')->passwordInput()->hint('Enter, your password')->label(Yii::t('common','Password again*')) ?>


<div class="form-group">
    <div class="col-lg-offset-2 col-lg-11">
        <?= Html::submitButton(Yii::t('common','Restore password'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
</div>
