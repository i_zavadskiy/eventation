<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 22.07.16
 * Time: 10:41
 */

namespace app\modules\users\models;

use yii;
use yii\db\ActiveRecord;
use yii\web\Session;

class NewPassword extends ActiveRecord
{
    public $password;
    public $password2;
    public $email;
    private $_user;


    public function rules()
    {
        return [
            [['password', 'password2', 'email'], 'required', 'message' => Yii::t('common','Required field!')],
            ['email','validateMail'],
            ['password2', 'compare', 'compareAttribute' => 'password', 'operator' => '=='],
        ];
    }

    public static function tableName()
    {
        return 'users';
    }

    public function sent()
    {
        $this->_user = $this->find()->where(['email' => $this->email])->one();

        Emailer::sendPass($this->email, $this->_user['hash']);

    }

    public function validateMail()
    {
        if(User::find()->where(['email' => $this->email])->one()) {
            return true;
        } else {
            $this->addError('email', Yii::t('common','there is no user with this email'));
        }
    }
}