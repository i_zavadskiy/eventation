<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 27.07.16
 * Time: 16:17
 */

namespace app\modules\users\models;

use app\models\ImageUpload;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class AvatarForm extends ActiveRecord
{
    /**
     * @var UploadedFile
     */

    public $imageFile;
    public $coordX;
    public $coordY;
    public $height;
    public $weight;
    public $per;
    public $user;

    public function rules()
    {
        return [

            [['imageFile'], 'file', 'extensions' => 'png, jpg', 'maxSize'=>5242880 ,'mimeTypes'=>"image/*"],
        ];
    }

    public static function tableName()
    {
        return 'users';
    }


    public function __construct(array $config, $model)
    {
        $this->user = $model;
        parent::__construct($config);
    }



    public function setName($name)
    {
        $this->user->img = $name;
        $this->user->save();
    }


}