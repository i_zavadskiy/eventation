<?php

namespace app\modules\users\models;

use yii\db\ActiveRecord;
use app\modules\events\models\Events;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    public static function tableName()
    {
        return 'users';
    }


    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['id' => 'event_id'])
            ->viaTable('users_events', ['user_id' => 'id']);
    }

    public function getFull()
    {
        if(isset($this->img)) {
        return "http://10.10.54.150/uploads/".$this->img;
        }else{
            return false;
        }
    }

    public function getThumb()
    {
        return "http://10.10.54.150/uploads/thumbnail/".$this->img;
    }

    public function getMedium()
    {
        return "http://10.10.54.150/uploads/medium/".$this->img;
    }

    /**
     * @inheritdoc
     */

    public static function findIdentity($id)
    {
        return static::findOne(["id" => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['role' => $token]);

    }


    public static function findByEmail($email)
    {

        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
