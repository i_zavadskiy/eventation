<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = false;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required','message' => Yii::t('common','Required field!')],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            ['email', 'checkUser'],
            ['email', 'checkStatus'],
        ];
    }


    public function validatePassword()
    {
        $this->_user = $this->getUser();

        if(md5($this->password) === ($this->_user['password'])) {
            return true;
        } else {
            $this->addError('password',  Yii::t('common', 'Incorrect username or password.'));
            return false;
        }

    }

    public function checkUser()
    {
        if($this->getUser() == false) {
            $this->addError('email',  Yii::t('common', 'This email dont register.'));
            return false;
        } else {
            return true;
        }
    }

    public function checkStatus()
    {
        $this->_user = $this->getUser();
        if($this->_user['status'] == 1) {

            return true;
        } else {
            $this->addError('email',  Yii::t('common', 'Confirm your email.'));
            return false;
        }
    }

    public function login()
    {
        if($this->validate()) {
            return Yii::$app->getUser()->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }


    public function getUser()
    {

        if($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }


}
