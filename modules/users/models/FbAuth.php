<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 07.09.16
 * Time: 15:34
 */

namespace app\modules\users\models;

use yii;

use yii\db\ActiveRecord;

class FbAuth extends ActiveRecord
{
//    public $password;
//    public $password2;
//
//    public function rules()
//    {
//
//        return [
//            [['password'], 'trim'],
//            [['password', 'password2'], 'required', 'message' => Yii::t('common','Required field!')],
//            ['password', 'string', 'min' => 6, 'max' => 45],
//            ['password2', 'compare', 'compareAttribute' => 'password', 'operator' => '=='],
//        ];
//    }

    public static function tableName()
    {
        return 'users';
    }



    public function beforeSave($insert)
    {
        $this->password = md5($this->email);
        $this->role = 1;
        $this->hash = md5($this->password.time());
        $this->status = 1;

        return parent::beforeSave($insert);
    }
}