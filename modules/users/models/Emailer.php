<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 19.07.16
 * Time: 12:59
 */

namespace app\modules\users\models;

use Yii;
use yii\helpers\Url;

class Emailer
{


    public static function send($adress, $hash)
    {

        $val = MailTable::find()->where(['type' => Yii::t('common','confirm')])->one();
        $url = Url::to(['/users/register/confirm', 'hash' => $hash]);
        Yii::$app->mailer->compose()
            ->setFrom('test.site.event@gmail.com')
            ->setTo($adress)
            ->setSubject($val['subject'])
            ->setTextBody($val['content'].'http://10.10.54.150'.$url)
            ->setHtmlBody($val['html_body'].'"http://10.10.54.150'.$url.'">'.Yii::t('common','CONFIRM').'</a> ')
            ->send();
    }

    public static function sendPass($adress, $hash)
    {
        //todo
        $val = MailTable::find()->where(['type' => Yii::t('common','password')])->one();
        $url = Url::to(['/users/auth/forgot-password', 'hash' => $hash]);

        Yii::$app->mailer->compose()
            ->setFrom('test.site.event@gmail.com')
            ->setTo($adress)
            ->setSubject($val['subject'])
            ->setTextBody($val['content'].'http://10.10.54.150'.$url)
            ->setHtmlBody($val['html_body'].'"http://10.10.54.150'.$url.'">RECOVER</a> ')
            ->send();
    }

    public static function sendRemind($adress, $id)
    {
        //todo

        $val = MailTable::find()->where(['type' => Yii::t('common','remind')])->one();
        $url = Url::to(['/events/one/'.$id]);

        Yii::$app->mailer->compose()
            ->setFrom('test.site.event@gmail.com')
            ->setTo($adress)
            ->setSubject($val['subject'])
            ->setTextBody($val['content'].'http://10.10.54.150'.$url)
            ->setHtmlBody($val['html_body'].'"http://10.10.54.150'.$url.'">your event</a> ')
            ->send();
    }


}