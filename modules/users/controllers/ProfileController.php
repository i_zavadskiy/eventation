<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 26.07.16
 * Time: 15:27
 */

namespace app\modules\users\controllers;

use app\models\ImageCrop;
use app\models\ImageUpload;
use app\modules\users\models\DataForm;
use app\modules\users\models\ImageCropUser;
use app\modules\users\models\PasswordForm;
use app\modules\users\models\AvatarForm;

use app\modules\users\models\User;
use app\scripts\AjaxValidator;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\Json;
use  yii\helpers\FileHelper;


class ProfileController extends Controller
{
    use AjaxValidator;

    private $_user = null;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['profile'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $modelData = new DataForm([], $this->_findModel());
        $modelPassword = new PasswordForm([], $this->_findModel());
        $modelAvatar = new AvatarForm([], $this->_findModel());

        if(Yii::$app->request->isAjax) {
            if($modelData->load(Yii::$app->request->post())) {
                $this->validateQuery($modelData);
            }
            if($modelPassword->load(Yii::$app->request->post())) {
                $this->validateQuery($modelPassword);
            }
            if($modelAvatar->load(Yii::$app->request->post())) {
                $this->validateQuery($modelAvatar);
            }
        }


        if(Yii::$app->request->isPost) {
            if($modelData->load(Yii::$app->request->post()) && $modelData->validate()) {
                $modelData->saveData();
            }

            if($modelPassword->load(Yii::$app->request->post()) && $modelPassword->validate()) {
                $modelPassword->savePass();
            }


            if($_FILES['AvatarForm']['name']['imageFile']) {
                $modelAvatar->load(Yii::$app->request->post());

                if (mime_content_type($_FILES['AvatarForm']['tmp_name']['imageFile']) == "image/jpeg"
                    || mime_content_type($_FILES['AvatarForm']['tmp_name']['imageFile']) == "image/png"
                ) {


                    if (Yii::$app->request->post('AvatarForm')['weight'] != null) {
                        $imageModel = new ImageUpload(['upload_dir' => 'uploads/',
                            'upload_url' => 'uploads/', 'param_name' => 'AvatarForm']);

                    } else {
                        $imageModel = new ImageUpload(['upload_dir' => 'uploads/',
                            'upload_url' => 'uploads/', 'param_name' => 'AvatarForm']);
                    }
                    $crop = new ImageCropUser($imageModel->file_name, UPLOAD_PATH . '/' . $imageModel->get_upload_path());

                    $crop->crop(Yii::$app->request->post('AvatarForm')['coordX'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['coordY'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['height'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['weight'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        100, 'thumb'
                    );
                    $crop->crop(Yii::$app->request->post('AvatarForm')['coordX'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['coordY'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['height'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        Yii::$app->request->post('AvatarForm')['weight'] * 100 / Yii::$app->request->post('AvatarForm')['per'],
                        800, 'medium'
                    );
                    $modelAvatar->setName($imageModel->file_name);
                }

                } elseif ($modelAvatar->load(Yii::$app->request->post())) {

                    $crop = new ImageCropUser($modelAvatar['user']['img'], UPLOAD_PATH . '/uploads/');

                    $crop->crop(Yii::$app->request->post('AvatarForm')['coordX'],
                        Yii::$app->request->post('AvatarForm')['coordY'],
                        Yii::$app->request->post('AvatarForm')['height'],
                        Yii::$app->request->post('AvatarForm')['weight'],
                        100, 'thumb'
                    );
                    $crop->crop(Yii::$app->request->post('AvatarForm')['coordX'],
                        Yii::$app->request->post('AvatarForm')['coordY'],
                        Yii::$app->request->post('AvatarForm')['height'],
                        Yii::$app->request->post('AvatarForm')['weight'],
                        800, 'medium'
                    );
                    //$modelAvatar->setName($imageModel->file_name);
                }

        }


        return $this->render('index', [
            'model' => $this->_findModel(),
            'modelData' => $modelData,
            'modelPassword' => $modelPassword,
            'modelAvatar' => $modelAvatar,
        ]);
    }

    /**
     * @return User the loaded model
     */
    private function _findModel()
    {

        if($this->_user == null) {
            $this->_user = User::findOne(Yii::$app->user->identity->getId());
        }
        return $this->_user;
    }

    public function actionProfile()
    {
        $model = User::findOne(Yii::$app->request->get('id'));
        return $this->render('user', ['model' => $model]);
    }

}