<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 20.07.16
 * Time: 15:13
 */

namespace app\modules\users\controllers;

use app\modules\users\models\FbAuth;
use app\modules\users\models\RegisterForm;
use app\scripts\AjaxValidator;
use app\modules\users\models\NewPassword;
use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\users\models\LoginForm;
use app\modules\users\models\User;


class AuthController extends Controller
{
    use AjaxValidator;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }
    public function oAuthSuccess($client) {

        $userAttributes = $client->getUserAttributes();
        $user = User::findByEmail($userAttributes['email']);
        if($user!=null){
            Yii::$app->getUser()->login($user);
        }else{
//            $auth = Yii::$app->session;
//            $auth->open();
//            $auth->set('email',$userAttributes['email']);
//            $auth->set('first_name',$userAttributes['first_name']);
//            $auth->set('last_name',$userAttributes['last_name']);
//            $auth->set('birthday',$userAttributes['birthday']);
//            $model = new FbAuth();
//            return $this->render('new-password',['model'=>$model]);


            $model = new FbAuth();
            $model->email = $userAttributes['email'];
            $model->firstname = $userAttributes['first_name'];
            $model->lastname = $userAttributes['last_name'];
            $model->dob = date("Ymd", strtotime($userAttributes['birthday']));
            $model->save();



            Yii::$app->session->setFlash('success', Yii::t('common','Welcome! Your password it is your email. Please change it in your profile'));
            $user = User::findByEmail($userAttributes['email']);
            Yii::$app->getUser()->login($user);
        }
    }

//    public function actionCreateFbUser(){
//        $user = new User();
//        $auth = Yii::$app->session;
//        $user->email = $auth->get('email');
//        $user->firstname = $auth->get('firstname');
//        $user->lastname = $auth->get('lastname');
//        $user->dob =  date("Ymd", strtotime($auth->get('dob')));
//        $user->role = 1;
//        $user->status = 1;
//        $user->password = md5(Yii::$app->request->post('password'));
//        $user->save();
//        Yii::$app->getUser()->login($user);
//    }
    public function actionLogin()
    {

        $model = new LoginForm();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            }

            if($model->login()) return $this->redirect('/');

        }
        return $this->renderAjax('login', ['model' => $model]);

    }

    public function actionLogout()
    {
        if(Yii::$app->user->logout()) {
            return $this->redirect('/');
        }

    }

    public function actionForgotPassword()
    {
        $model = new NewPassword();


        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if($model->password != null) {
                $modelUser = User::find()->where(['hash' => Yii::$app->request->get('hash')])->one();
                $modelUser->password = md5(Yii::$app->request->post('NewPassword')['password']);
                $modelUser->save();
                Yii::$app->session->setFlash('success', Yii::t('common','Password changed'));
                return $this->redirect('/');
            }

            if($model->validateMail()) {
                $model->sent();
                Yii::$app->session->setFlash('success', Yii::t('common','Check your mailbox for continue'));
                return $this->redirect('/');
            }
        } else {
            if(User::find()->where(["hash" => Yii::$app->request->get('hash')])->one()) {
                return $this->render('ForgotPassword', ['model' => $model,'hash'=>Yii::$app->request->get('hash')]);
            }
        }
        return $this->render('ForgotPassword', ['model' => $model]);

    }


}