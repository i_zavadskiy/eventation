<?php
/**
 * Created by PhpStorm.
 * User: zavadskyi
 * Date: 20.07.16
 * Time: 10:38
 */
namespace app\modules\users\controllers;

use app\modules\users\models\User;
use app\scripts\AjaxValidator;
use yii;
use yii\web\Controller;
use app\modules\users\models\RegisterForm;


class RegisterController extends Controller
{
    use AjaxValidator;

    public function actionForm()
    {
        $model = new RegisterForm();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax) {
                $this->validateQuery($model);
            } elseif($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('common','Confirm mail'));

                return $this->redirect('/');
            }
        }
        echo $this->renderAjax('register', ['model' => $model]);
    }


    public function actionConfirm()
    {
        if(User::find()->where(["hash" => Yii::$app->request->get('hash')])->one()) {
            $model = User::find()->where(["hash" => Yii::$app->request->get('hash')])->one();
            $model->status = 1;
            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('common','Congratulations! Your email is fine! =)'));
            return $this->redirect('/');
        } else {
            Yii::$app->session->setFlash('error', Yii::t('common','There is some mistake. Try to register again'));
            return $this->redirect('/');
        }
    }


}