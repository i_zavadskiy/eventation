
-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: event_db
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','11',1472633667),('admin','2',1471610450),('admin','21',1472225274),('admin','29',1472720928),('user','11',1472633673),('user','28',1472629559),('user','3',1471611508),('user','36',1473672496),('user','37',1473763668),('user','38',1473946815),('user','39',1474035138),('user','46',1474297135),('user','47',1474297263);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'Admin',NULL,NULL,1471354331,1471354331),('forAdmin',2,NULL,NULL,NULL,1471354331,1471354331),('forUser',2,NULL,NULL,NULL,1471354331,1471354331),('user',1,'User',NULL,NULL,1471354331,1471354331);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','forAdmin'),('user','forUser');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claims`
--

DROP TABLE IF EXISTS `claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claims` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_id` (`comment_id`),
  CONSTRAINT `claims_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claims`
--

LOCK TABLES `claims` WRITE;
/*!40000 ALTER TABLE `claims` DISABLE KEYS */;
/*!40000 ALTER TABLE `claims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relative` int(11) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `html_body` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `event_id` (`event_id`),
  KEY `relative` (`relative`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`relative`) REFERENCES `comments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (2,NULL,'1','','2016-09-13 10:25:16','2016-09-13 10:25:16',2,59),(3,NULL,'2','','2016-09-13 10:25:21','2016-09-13 10:25:21',2,59),(4,NULL,'3','','2016-09-13 10:25:24','2016-09-13 10:25:24',2,59),(5,NULL,'4','','2016-09-13 10:31:07','2016-09-13 10:31:07',2,59),(9,3,'2 1','','2016-09-13 11:15:02','2016-09-13 11:15:02',2,59),(10,2,'1 1','','2016-09-13 11:15:18','2016-09-13 11:15:18',2,59),(11,2,'1 2','','2016-09-13 11:15:27','2016-09-13 11:15:27',2,59),(12,2,'1 3','','2016-09-13 11:15:33','2016-09-13 11:15:33',2,59),(13,2,'1 4','','2016-09-13 11:15:41','2016-09-13 11:15:41',2,59),(14,4,'3 1','','2016-09-13 11:15:53','2016-09-13 11:15:53',2,59),(17,NULL,'1','','2016-09-13 11:17:48','2016-09-13 11:17:48',2,58),(20,17,'1','','2016-09-13 11:17:57','2016-09-13 11:17:57',2,58),(22,17,'2','','2016-09-13 11:18:56','2016-09-13 11:18:56',2,58),(24,5,'23','','2016-09-16 10:37:12','2016-09-16 10:37:12',2,59),(25,5,'ttrtrt','','2016-09-16 10:39:18','2016-09-16 10:39:18',29,59),(30,NULL,'12','','2016-09-16 10:42:38','2016-09-16 10:42:38',2,58),(31,NULL,'ыс','','2016-09-19 05:54:25','2016-09-19 05:54:25',2,55);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `content` mediumtext NOT NULL,
  `html_body` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES (1,'confirm','Confirm email','Hello!\n\nYou already registred on Eventation - the best site for events!\nPlease, follow this link to confirm your email:','Hello!</br>\n\nYou already registred on Eventation - the best site for events!\nPlease, follow this link to confirm your email:</br><a href=','2016-09-19 14:27:30','0000-00-00 00:00:00'),(2,'password','Recover password','Click on this link to continue ','<p>Click on this link to continue: </p> <a href=','2016-09-19 14:27:33','0000-00-00 00:00:00'),(3,'remind','Your event will be soon','Click on this link to continue ','<p>Click on this link to continue: </p> <a href=','2016-09-19 14:54:40','0000-00-00 00:00:00'),(4,'подтверждение','Подтвердите почту','Здравствуйте!\n\nВы только что зарегестрировались на сайте Eventation - лучшем ресурсе для ваших событий!\nПожалуйста, перейдите по сслке, чтобы подтвердить ваш email:','Здравствуйте!</br>\n\nВы только что зарегестрировались на сайте Eventation - лучшем ресурсе для ваших событий!\nПожалуйста, перейдите по сслке, чтобы подтвердить ваш email:</br><a href=','2016-09-19 14:49:06','0000-00-00 00:00:00'),(5,'пароль','Восстановление пароля','Нажмите на эту сслку дял продолжения','<p>Нажмите на эту ссылку для продолжения: </p> <a href=','2016-09-19 14:52:17','0000-00-00 00:00:00'),(6,'напоминание','Ваше событие скоро начнется','Нажмите на эту сслку дял продолжения','<p>Нажмите на эту ссылку для продолжения: </p> <a href=','2016-09-19 14:55:16','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `content` mediumtext NOT NULL,
  `date_registr` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_action` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `author_id` int(11) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (53,'Топ событие ин зе ворлд','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-09-15 13:50:42','2016-09-12 13:27:08',2,0,'pr2.jpg',0,50.317408112619,34.837646484375,'2016-09-12 10:27:09','2016-09-12 10:27:09'),(54,'Очередная встречка, че.','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-09-12 13:28:29','2016-10-12 10:35:59',2,0,NULL,0,49.809631563563,29.46533203125,'2016-09-12 10:28:33','2016-09-12 10:28:33'),(55,'THIS IS SPARTAAAAAAAAAAAA!!!!!!!!!!!','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-10-12 07:30:59','2016-11-09 14:50:59',2,1,NULL,3,52.308478623663,23.09326171875,'2016-09-12 10:33:24','2016-09-19 07:24:11'),(56,'WOW IS IT REAL? COME HERE!','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-10-05 07:30:06','2016-10-11 05:45:06',29,1,'niy (7).png',1,7.6892171277362,69.2138671875,'2016-09-12 10:36:18','2016-09-20 07:13:22'),(57,'Ивенты заказывали?!','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-10-11 06:45:24','2016-10-18 13:45:24',29,0,'onetile-402 (22).png',0,47.75409797968,27.894287109375,'2016-09-12 10:41:51','2016-09-12 10:41:51'),(58,'Здесь могла быть ваша реклама','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-09-22 11:50:26','2016-09-28 14:50:26',29,0,'1452736497_yumor12 (26).jpg',0,54.072282655604,22.928466796875,'2016-09-12 10:43:58','2016-09-19 04:38:13'),(59,'Это на эльфийском?!','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,','2016-09-21 13:55:42','2016-09-22 13:50:42',29,1,'194633_1280_1024.jpg',4,52.261433597272,23.04931640625,'2016-09-12 10:48:24','2016-09-19 12:09:41');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('',1472807310),('m000000_000000_base',1471358392),('m160715_124734_init_migration',1471359756),('m160831_090027_init_mail_table',1472634988),('m160901_091743_cteate_claim_table',1472722458),('m160902_090439_add_reletive_column_in_comment_table',1472819070),('m160905_142836_create_rating_column_in_event_table',1473085942),('m160905_144533_create_vote_event_table',1473086945),('m160908_083828_create_report_table',1473324211),('m160913_093641_add_count_column_in_events_table',1473759478);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (5,'Doesnt work','Dates','/en/events/calendar/show','2016-09-08 09:13:14','0000-00-00 00:00:00'),(7,'Bug','fsd+fdsfdfdsf','/en/events/show/49','2016-09-08 12:35:14','0000-00-00 00:00:00'),(8,'Mistake','rtmjmj','/en','2016-09-09 10:26:56','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `dob` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `hash` varchar(45) NOT NULL,
  `img` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'mailbox85lvl@gmail.com2','96e79218965eb72c92a549dd5a330112','Admin','Ivanovich','2016-09-19 08:03:49',2,1,'c180b1d43efc57de152ca243385b4aa7','wallpapersdota2.com-432 (32).jpg','31','0000-00-00 00:00:00','2016-08-21 21:00:00'),(11,'hd9fusf@fdsf.sdf','4297f44b13955235245b2497399d7a93','Troll','Warlord','2016-08-21 21:00:00',1,1,'5a445bb66a636a5515bd416cb7532045','troll_warlord_vert.jpg','','2016-08-21 21:00:00','2016-08-30 21:00:00'),(12,'2131@gdf.hfg','4297f44b13955235245b2497399d7a93','Faceless','Void','2016-08-21 21:00:00',1,1,'0637985d6189d620f432a72505c7f041','faceless_void_vert.jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(13,'123123@fsd.fds','4297f44b13955235245b2497399d7a93','Naga','Siren','2016-08-21 21:00:00',1,1,'54d219b693ce635cf1ade6bbc5ff75d4','niy (1).png','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(14,'afdfds@fdsfds.fdsf','4297f44b13955235245b2497399d7a93','Chaos','Knight','2016-08-22 10:38:51',1,1,'733b4f179e95376bc59453b5dfd85ad2','3415478-8656016879-dota2.jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(15,'lol2fds@das.a','4297f44b13955235245b2497399d7a93','Cristal','Maiden','2016-08-02 21:00:00',1,1,'552a1b51e92b2a410037e0beea44df69','773.jpeg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(16,'lolo@dsa.das','4297f44b13955235245b2497399d7a93','Witch','Dorctor','2016-08-03 21:00:00',1,1,'6bc9ceba3912a02dac91609c79d04cd4','af949ad91b8719e182c8e617c1e5569a.jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(17,'fdasfds@fd.fsd','4297f44b13955235245b2497399d7a93','Shtorm','Spirit','2016-08-04 21:00:00',2,1,'7caaee6a1b100f4cedd9263802cbeac5','wallpapersdota2.com-432.jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(18,'asdsA@fddfs.fsd','4297f44b13955235245b2497399d7a93','Ember','Spirit','2016-08-16 21:00:00',2,1,'f2da60b14dd3dd4dbf56aa1bc064b7ee','maxresdefault.jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(19,'dasdsa@ddsf.fsd','4297f44b13955235245b2497399d7a93','Erth','Spirit','2016-08-05 21:00:00',2,1,'81a3915e05f591a15ee8bc6639b0e344','OAyeLHx.png','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(20,'dasdasd@dsf.fsd','4297f44b13955235245b2497399d7a93','Death','Prophet','2016-08-07 21:00:00',1,1,'433dd01cb7006592d8ecc41bf82bb52d','maxresdefault (1).jpg','','2016-08-21 21:00:00','2016-08-21 21:00:00'),(22,'adsds@dsads.fsdf','4297f44b13955235245b2497399d7a93','fddsfd','sdfsdfdsf','2016-08-09 21:00:00',1,1,'dc74da7a7c8d5b5d1749531e5d1e4633','wallpapersdota2.com-432.jpg','','2016-08-29 21:00:00','2016-08-29 21:00:00'),(23,'fdsfs@sfd.fds','4297f44b13955235245b2497399d7a93','sddfsdfs','dfsdfsdfsdfs','2016-08-09 21:00:00',1,1,'6523543c0b2bf1215092f2ee33bd1a28','pr.jpg','','2016-08-29 21:00:00','2016-08-29 21:00:00'),(24,'dasdas@fsdf.sdf','4297f44b13955235245b2497399d7a93','fsdfsd','fdsfsdfdsf','2016-08-22 21:00:00',1,1,'401b25c07043a22283b3ab67fec1dc25','maxresdefault (4).jpg','','2016-08-29 21:00:00','2016-08-29 21:00:00'),(25,'fdfdfs@fdf.vds','4297f44b13955235245b2497399d7a93','fdsfds','fdsfds','2016-08-17 21:00:00',1,1,'01ed5a5b2ce80015546dc2595c1a2bed',NULL,'','2016-08-29 21:00:00','2016-08-29 21:00:00'),(26,'dsfsdf@fsdfds.fsdf','4297f44b13955235245b2497399d7a93','fdsdfsf','fdsfdsfs','2016-08-17 21:00:00',1,1,'6af772f0bf96f67f2880062305f8579c','pr.jpg','','2016-08-29 21:00:00','2016-08-29 21:00:00'),(27,'fddf@fdsfd.fsd','4297f44b13955235245b2497399d7a93','dfsfsf','dfssddfs','2016-08-08 21:00:00',1,1,'06e8576e2df5be62a528ccd3bc4384f6','troll_warlord_vert (1).jpg','','2016-08-29 21:00:00','2016-08-29 21:00:00'),(28,'dfafd@fds.fsd','4297f44b13955235245b2497399d7a93','123123','123123','2016-08-22 21:00:00',1,0,'bd165d64bec7cf0c7e983fd840531072',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'123@123.123','4297f44b13955235245b2497399d7a93','new','user','2016-09-16 08:42:44',2,1,'6b292b9e2913960177aa35740fb8228f','pr2 (8).jpg','Lol','0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'brj@ukr.net','e10adc3949ba59abbe56e057f20f883e','Bogdan','Test','2016-09-11 21:00:00',1,1,'2954cb4a0d44c6a04f496a3b6b1fbf6d','0-02-01-0b023896105ad2853d6af016e4ada99b7aa05','','0000-00-00 00:00:00','2016-09-11 21:00:00'),(37,'arhipov.vladislav@inbox.ru','e10adc3949ba59abbe56e057f20f883e','Vladislav','Arhipov','2016-09-13 10:48:23',1,1,'04b31bd134466244697fb40628350fb9',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'dadad@dasd.dasd','4297f44b13955235245b2497399d7a93','dfdsdfs','fdsdsfsdsd','2016-09-05 05:25:23',1,0,'b39dcf3cc053f7b8c38e1408060f11b2',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,'mailbox85lvl@gmail.com','4297f44b13955235245b2497399d7a93','fddfdsf','fsdf dsfsdf','2016-09-19 15:01:16',1,1,'abdf9d4b0c6850817355791ae3d0795c',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_events`
--

DROP TABLE IF EXISTS `users_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `date_confirm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `users_events_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_events_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_events`
--

LOCK TABLES `users_events` WRITE;
/*!40000 ALTER TABLE `users_events` DISABLE KEYS */;
INSERT INTO `users_events` VALUES (43,2,59,'2016-09-19 08:05:11'),(44,2,56,'2016-09-19 08:05:40'),(45,2,55,'2016-09-19 08:06:01'),(46,47,59,'2016-09-19 15:08:00'),(47,29,59,'2016-09-19 15:09:41'),(48,29,56,'2016-09-20 10:13:22');
/*!40000 ALTER TABLE `users_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vote_event`
--

DROP TABLE IF EXISTS `vote_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_event`
--

LOCK TABLES `vote_event` WRITE;
/*!40000 ALTER TABLE `vote_event` DISABLE KEYS */;
INSERT INTO `vote_event` VALUES (38,47,59,5,'2016-09-19 15:09:09','0000-00-00 00:00:00'),(39,2,59,4,'2016-09-19 15:09:17','0000-00-00 00:00:00'),(40,29,59,5,'2016-09-19 15:09:41','0000-00-00 00:00:00'),(41,29,56,1,'2016-09-20 10:13:22','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vote_event` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-28 12:45:48
