<?php

use \app\modules\events\models\Events;
use \app\modules\users\models\User;
use Yii;
class ExampleTest extends \Codeception\Test\Unit
{

    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $event = new Events();
        $user = User::findOne(20);
        Yii::$app->getUser()->login($user);
       $event->name = "test";
        $this->assertTrue($event->validate(['uname']));
       $event->content = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,";
        $this->assertTrue($event->validate(['content']));
       $event->date_registr = date('Y-m-d H:i:s', strtotime('2016-10-05 10:30:06'));
        $this->assertTrue($event->validate(['date_registr']));
       $event->date_action = date('Y-m-d H:i:s', strtotime('2016-10-05 10:30:06'));
       $this->assertTrue($event->validate(['date_action']));

        $event->author_id = "29";


        $event->count = "1";

        $event->img = null;

        $event->rating = "4";

        $event->lat = "52.261433597272";

        $event->lng = "55";

        $event->save();

       $this->tester->seeInDatabase('events', ['name' => 'test']);

    }
}