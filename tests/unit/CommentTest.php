<?php


class CommentTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testComment()
    {
        $comment = new \app\modules\events\models\Comments();

        $comment->content = 'Some text';
        $comment->user_id = 20;
        $comment->setPostId(54);
        $comment->save();

        $this->tester->seeInDatabase('comments', ['content' => 'Some text']);

    }
}