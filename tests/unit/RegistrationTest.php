<?php

use \app\modules\users\models\RegisterForm;
class RegistrationTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testRegisterGood()
    {
        $user = new RegisterForm();

        $user->email = 'newmail@fd.fd';
        $this->assertTrue($user->validate(['email']));

        $user->password = md5('123123');
        $user->password2 = md5('123123');
        $this->assertTrue($user->validate(['password']));

        $user->firstname = 'testName';
        $this->assertTrue($user->validate(['firstname']));

        $user->lastname = 'testName2';
        $this->assertTrue($user->validate(['lastname']));

        $user->dob = date('Y-m-d H:i:s', strtotime('2000-10-05 10:30:06'));
        $this->assertTrue($user->validate(['dob']));

        $user->role = 1;
        $user->status = 1;
        $user->save();

        $this->tester->seeInDatabase('users', ['firstname' => 'testName']);
    }
    public function testRegisterValidator()
    {
        $user = new RegisterForm();

        $user->email = 'newmailfd.fd';
        $this->assertFalse($user->validate(['email']));
        $user->email = 'newm@ailfdfd@';
        $this->assertFalse($user->validate(['email']));

        $user->firstname = 'testNamefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
        $this->assertFalse($user->validate(['firstname']));

        $user->lastname = 'testNamefffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
        $this->assertFalse($user->validate(['lastname']));

        $user->dob = date('Y-m-d H:i:s', strtotime('2020-10-05 10:30:06'));
        $this->assertFalse($user->validate(['dob']));

        $user->role = 1;
        $user->status = 1;
        $this->assertFalse($user->save());

        $this->tester->cantSeeInDatabase('users', ['lastname' => 't']);
    }
}